-keep class io.ychescale9.weatherapp.** { *; }
-keepattributes *Annotation*
-keepattributes EnclosingMethod

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

# Dagger Android
-dontwarn dagger.android.**

# Gson
-keepattributes Signature
-keep class sun.misc.Unsafe { *; }
-keep class io.ychescale9.weatherapp.data.model.** { *; }


# joda time
-dontwarn org.joda.convert.**
-dontwarn org.joda.time.**
-keep class org.joda.time.** { *; }
-keep interface org.joda.time.** { *;}

# OkHttp
-dontwarn okhttp3.**
-dontwarn okio.**

# Retrofit2
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

# Espresso
-keep class android.support.test.espresso.IdlingResource { *; }

# LeakCanary
-dontwarn com.squareup.haha.guava.**
-dontwarn com.squareup.haha.perflib.**
-dontwarn com.squareup.haha.trove.**
-dontwarn com.squareup.leakcanary.**
-keep class com.squareup.haha.** { *; }
-keep class com.squareup.leakcanary.** { *; }
