package io.ychescale9.weatherapp;

import timber.log.Timber;

/**
 * Created by yang on 9/12/16.
 */
public class WeatherAppApplication extends BaseWeatherAppApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        // install the release tree
        Timber.plant(new ReleaseTree());
    }
}
