package io.ychescale9.weatherapp.data.api;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import io.reactivex.Observable;
import io.ychescale9.weatherapp.data.MockData;
import io.ychescale9.weatherapp.data.MockDataHelper;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.ForecastWeatherCondition;
import io.ychescale9.weatherapp.data.serializer.CurrentWeatherConditionJsonDeserializer;
import io.ychescale9.weatherapp.data.serializer.ForecastWeatherConditionJsonDeserializer;
import io.ychescale9.weatherapp.data.serializer.ForecastWeatherTypeAdapterFactory;
import retrofit2.mock.BehaviorDelegate;
import timber.log.Timber;

/**
 * Created by yang on 9/12/16.
 */
public class MockWeatherAppService implements WeatherAppService {

    private final BehaviorDelegate<WeatherAppService> delegate;
    private final CurrentWeatherConditionJsonDeserializer currentWeatherConditionJsonDeserializer;
    private final ForecastWeatherConditionJsonDeserializer forecastWeatherConditionJsonDeserializer;

    public MockWeatherAppService(@NonNull BehaviorDelegate<WeatherAppService> delegate,
                                 @NonNull CurrentWeatherConditionJsonDeserializer currentWeatherConditionJsonDeserializer,
                                 @NonNull ForecastWeatherConditionJsonDeserializer forecastWeatherConditionJsonDeserializer) {
        this.delegate = delegate;
        this.currentWeatherConditionJsonDeserializer = currentWeatherConditionJsonDeserializer;
        this.forecastWeatherConditionJsonDeserializer = forecastWeatherConditionJsonDeserializer;
    }

    @Override
    public Observable<CurrentWeatherCondition> loadCurrentWeatherCondition(double latitude, double longitude) {
        // create Gson object with custom deserializer
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(CurrentWeatherCondition.class, currentWeatherConditionJsonDeserializer);
        Gson gson = gsonBuilder.create();
        // convert the mock data from json to CurrentWeatherCondition
        CurrentWeatherCondition currentWeatherCondition = gson.fromJson(MockData.CURRENT_WEATHER_CONDITION_JSON, CurrentWeatherCondition.class);
        // offset the date time of the mock currentWeatherCondition so it's greater than the current time.
        currentWeatherCondition = MockDataHelper.offsetCurrentWeatherConditionDateTimeFromNow(currentWeatherCondition);

        Timber.d("Generated mock CurrentWeatherCondition.");

        return delegate.returningResponse(currentWeatherCondition).loadCurrentWeatherCondition(latitude, longitude);
    }

    @Override
    public Observable<List<ForecastWeatherCondition>> loadForecastWeatherConditions(double latitude, double longitude) {
        // create Gson object with custom deserializer
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(ForecastWeatherCondition.class, forecastWeatherConditionJsonDeserializer);
        gsonBuilder.registerTypeAdapterFactory(new ForecastWeatherTypeAdapterFactory());
        Gson gson = gsonBuilder.create();
        // convert the mock data from json to List<ForecastWeatherCondition>
        Type listType = new TypeToken<List<ForecastWeatherCondition>>() {}.getType();
        List<ForecastWeatherCondition> forecastWeathers = gson.fromJson(MockData.FORECAST_WEATHER_CONDITIONS_JSON, listType);
        // offset the date time of the mock forecastWeathers so they are all greater than the current time.
        forecastWeathers = MockDataHelper.offsetForecastWeatherConditionsDateTimeFromNow(forecastWeathers);

        Timber.d("Generated mock ForecastWeatherConditions.");

        return delegate.returningResponse(forecastWeathers).loadForecastWeatherConditions(latitude, longitude);
    }
}
