package io.ychescale9.weatherapp.data;

import android.support.annotation.NonNull;

import org.joda.time.DateTime;

import java.util.List;

import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.ForecastWeatherCondition;

/**
 * Created by yang on 13/12/16.
 */
public class MockDataHelper {

    /** The offset time in milliseconds between the forecast data and "now" **/
    private static final long FORECAST_DATE_TIME_OFFSET_FROM_NOW = 30 * 60 * 1000; // 30 minutes

    /**
     * Offset the dateTime of the currentWeatherCondition passed in so it's AFTER the current time.
     * It will return the object if the currentWeatherCondition's datetime is already "in the future".
     * @param currentWeatherCondition
     * @return
     */
    public static CurrentWeatherCondition offsetCurrentWeatherConditionDateTimeFromNow(@NonNull CurrentWeatherCondition currentWeatherCondition) {
        long now = DateTime.now().getMillis();
        // check if the CurrentWeatherCondition is already in the future
        if (currentWeatherCondition.dateTimeMillis >= now + FORECAST_DATE_TIME_OFFSET_FROM_NOW) {
            return currentWeatherCondition;
        }

        // apply offset to every CurrentWeatherCondition
        currentWeatherCondition.dateTimeMillis = now + FORECAST_DATE_TIME_OFFSET_FROM_NOW;

        return currentWeatherCondition;
    }

    /**
     * Offset the dateTime of the forecastWeatherCondition objects passed in so they are all AFTER the current time.
     * It will return the same list if the first forecastWeatherCondition's datetime is already "in the future".
     * Assumes that the forecastWeatherConditions passed in are in chronological order.
     * @param forecastWeatherConditions
     * @return
     */
    public static List<ForecastWeatherCondition> offsetForecastWeatherConditionsDateTimeFromNow(@NonNull List<ForecastWeatherCondition> forecastWeatherConditions) {
        if (forecastWeatherConditions.size() == 0) {
            return forecastWeatherConditions;
        }

        long now = DateTime.now().getMillis();
        long firstForecastWeatherConditionDateTime = forecastWeatherConditions.get(0).dateTimeMillis;
        // check if the first ForecastWeatherCondition is already in the future
        if (firstForecastWeatherConditionDateTime >= now + FORECAST_DATE_TIME_OFFSET_FROM_NOW) {
            return forecastWeatherConditions;
        }

        long relativeOffset = now + FORECAST_DATE_TIME_OFFSET_FROM_NOW - firstForecastWeatherConditionDateTime;
        // iterate through the list and apply the same offset to every ForecastWeatherCondition
        for (ForecastWeatherCondition forecastWeatherCondition : forecastWeatherConditions) {
            forecastWeatherCondition.dateTimeMillis += relativeOffset;
        }

        return forecastWeatherConditions;
    }
}
