package io.ychescale9.weatherapp.data.api;

/**
 * Created by yang on 9/12/16.
 * Provides methods for getting api constant values
 */
public class ApiConstants extends BaseApiConstants {

    @Override
    public String getApiBaseUrl() {
        return "http://example.com";
    }

    @Override
    public int getConnectTimeOutMilliseconds() {
        return 0;
    }

    @Override
    public int getReadTimeOutMilliseconds() {
        return 0;
    }

    @Override
    public int getWriteTimeOutMilliseconds() {
        return 0;
    }
}
