package io.ychescale9.weatherapp.data.api;

import android.location.Address;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.weatherapp.data.MockData;
import io.ychescale9.weatherapp.data.model.SimpleAddress;
import io.ychescale9.weatherapp.data.serializer.CurrentWeatherConditionJsonDeserializer;
import io.ychescale9.weatherapp.data.serializer.ForecastWeatherConditionJsonDeserializer;
import io.ychescale9.weatherapp.util.GeocodingHelper;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

import static io.ychescale9.weatherapp.data.helper.ModelUtil.toSimpleAddress;

/**
 * Created by yang on 9/12/16.
 */
@Module
public class ApiModule {

    @Provides
    @Singleton
    Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("http://example.com")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    WeatherAppService provideWeatherAppService(Retrofit retrofit,
                                               CurrentWeatherConditionJsonDeserializer currentWeatherJsonDeserializer,
                                               ForecastWeatherConditionJsonDeserializer forecastWeatherJsonDeserializer) {
        NetworkBehavior behavior = NetworkBehavior.create();
        // no delay
        behavior.setDelay(0, TimeUnit.MILLISECONDS);
        // no failure
        behavior.setFailurePercent(0);
        MockRetrofit mockRetrofit = new MockRetrofit.Builder(retrofit)
                .networkBehavior(behavior)
                .build();
        BehaviorDelegate<WeatherAppService> delegate = mockRetrofit.create(WeatherAppService.class);

        return new MockWeatherAppService(delegate, currentWeatherJsonDeserializer, forecastWeatherJsonDeserializer);
    }

    @Provides
    @Singleton
    GeocodingHelper provideGeocodingHelper() {
        // create a mock address
        final Address mockAddress = new Address(Locale.getDefault());
        // we only care about the locality (city name)
        mockAddress.setLocality(MockData.CITY_NAME);
        mockAddress.setLongitude(MockData.COORDINATES.getLongitude());
        mockAddress.setLatitude(MockData.COORDINATES.getLatitude());
        return new GeocodingHelper() {

            @Override
            public List<SimpleAddress> getFromLocation(double latitude, double longitude, int maxResults) throws IOException {

                return Collections.singletonList(toSimpleAddress(mockAddress));
            }
        };
    }
}
