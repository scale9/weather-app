package io.ychescale9.weatherapp;

import io.ychescale9.weatherapp.data.MockData;
import io.ychescale9.weatherapp.data.model.Coordinates;

/**
 * Created by yang on 15/12/16.
 */
public class AppEnvironment extends BaseAppEnvironment {

    @Override
    public Coordinates getFixedLocation() {
        // provide a mock location as the fixed location for dev build
        return MockData.COORDINATES;
    }
}
