package io.ychescale9.weatherapp.presentation.util;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by yang on 11/12/16.
 */
public final class FontUtils {

    public static Typeface getWeatherIconsTypeface(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/weathericons.ttf");
    }
}
