package io.ychescale9.weatherapp.presentation.home;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.weatherapp.data.api.ApiConstants;
import io.ychescale9.weatherapp.data.cache.CurrentWeatherConditionCache;
import io.ychescale9.weatherapp.data.cache.DailyForecastWeatherConditionsCache;
import io.ychescale9.weatherapp.data.disk.SharedPreferencesManager;
import io.ychescale9.weatherapp.data.repository.WeatherAppRepository;
import io.ychescale9.weatherapp.domain.LoadCurrentWeatherCondition;
import io.ychescale9.weatherapp.domain.LoadDailyForecastWeatherConditions;
import io.ychescale9.weatherapp.domain.ReverseGeocode;
import io.ychescale9.weatherapp.presentation.helper.PresenterManager;
import io.ychescale9.weatherapp.presentation.helper.WeatherIconMapper;
import io.ychescale9.weatherapp.util.Clock;
import io.ychescale9.weatherapp.util.GeocodingHelper;
import io.ychescale9.weatherapp.util.SchedulerProvider;

/**
 * Created by yang on 9/12/16.
 */
@Module
public class HomeModule {

    @Provides
    HomeContract.View provideHomeView(HomeActivity activity) {
        return activity;
    }

    @Provides
    HomeContract.Presenter provideHomePresenter(HomeContract.View homeView,
                                                SharedPreferencesManager sharedPreferencesManager,
                                                Clock clock,
                                                ReverseGeocode reverseGeocode,
                                                LoadCurrentWeatherCondition loadCurrentWeatherCondition,
                                                LoadDailyForecastWeatherConditions loadDailyForecastWeatherConditions,
                                                ApiConstants apiConstants) {
        HomeContract.Presenter presenter;
        presenter = (HomeContract.Presenter) PresenterManager.getInstance().restorePresenter(homeView.getClass());
        if (presenter == null) {
            presenter = new HomePresenter(
                    homeView,
                    sharedPreferencesManager,
                    clock,
                    reverseGeocode,
                    loadCurrentWeatherCondition,
                    loadDailyForecastWeatherConditions,
                    apiConstants.getAutoRefreshIntervalMilliseconds(),
                    apiConstants.getForecastDataUpdateIntervalMilliseconds()
            );
        } else {
            presenter.setView(homeView);
        }
        return presenter;
    }

    @Provides
    ReverseGeocode provideReverseGeocode(SchedulerProvider schedulerProvider,
                                         GeocodingHelper geocodingHelper) {
        return new ReverseGeocode(
                schedulerProvider,
                geocodingHelper
        );
    }

    @Provides
    LoadCurrentWeatherCondition provideLoadCurrentWeatherCondition(SchedulerProvider schedulerProvider,
                                                                   WeatherAppRepository weatherAppRepository,
                                                                   CurrentWeatherConditionCache currentWeatherConditionCache) {
        return new LoadCurrentWeatherCondition(
                schedulerProvider,
                weatherAppRepository,
                currentWeatherConditionCache
        );
    }

    @Provides
    LoadDailyForecastWeatherConditions provideLoadDailyForecastWeatherConditions(SchedulerProvider schedulerProvider,
                                                                                 WeatherAppRepository weatherAppRepository,
                                                                                 DailyForecastWeatherConditionsCache dailyForecastWeatherConditionsCache) {
        return new LoadDailyForecastWeatherConditions(
                schedulerProvider,
                weatherAppRepository,
                dailyForecastWeatherConditionsCache
        );
    }

    @Provides
    WeatherIconMapper provideWeatherIconMapper(Clock clock) {
        return new WeatherIconMapper(clock);
    }
}
