package io.ychescale9.weatherapp.presentation.helper;

import android.support.annotation.VisibleForTesting;
import android.support.v4.util.SimpleArrayMap;

import java.util.ArrayList;
import java.util.List;

import io.ychescale9.weatherapp.presentation.base.IPresenter;

/**
 * Created by yang on 9/12/16.
 */
public class PresenterManager {

    private static PresenterManager instance;

    private final SimpleArrayMap<Class, IPresenter> presenters;

    PresenterManager() {
        presenters = new SimpleArrayMap<>();
    }

    public static PresenterManager getInstance() {
        if (instance == null) {
            instance = new PresenterManager();
        }
        return instance;
    }

    public IPresenter restorePresenter(Class c) {
        return presenters.get(c);
    }

    public void savePresenter(Class c, IPresenter presenter) {
        presenters.put(c, presenter);
    }

    public void removePresenter(Class c) {
        presenters.remove(c);
    }

    @VisibleForTesting
    protected void clear() {
        presenters.clear();
    }

    @VisibleForTesting
    List<IPresenter> getAllPresenters() {
        List<IPresenter> allPresenters = new ArrayList<>(presenters.size());
        for (int index = 0; index < presenters.size(); index++) {
            allPresenters.add(presenters.get(presenters.keyAt(index)));
        }
        return allPresenters;
    }
}
