package io.ychescale9.weatherapp.presentation.home;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.joda.time.DateTime;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.ychescale9.weatherapp.AppEnvironment;
import io.ychescale9.weatherapp.R;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.SummaryForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.Temperature;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;
import io.ychescale9.weatherapp.presentation.base.BaseActivity;
import io.ychescale9.weatherapp.presentation.helper.WeatherIconMapper;
import io.ychescale9.weatherapp.presentation.util.FontUtils;
import io.ychescale9.weatherapp.util.Clock;
import io.ychescale9.weatherapp.util.GoogleApiUtils;
import io.ychescale9.weatherapp.util.WeakLocationListener;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import timber.log.Timber;

import static com.google.android.gms.location.LocationServices.FusedLocationApi;

/**
 * Created by yang on 9/12/16.
 */
public class HomeActivity extends BaseActivity<HomeContract.Presenter> implements HomeContract.View, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private final static String FINE_LOCATION_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION;
    private final static String COARSE_LOCATION_PERMISSION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private final static String[] LOCATION_PERMISSIONS = {
            FINE_LOCATION_PERMISSION,
            COARSE_LOCATION_PERMISSION
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1;

    @Inject
    HomeContract.Presenter presenter;

    @Inject
    WeatherIconMapper weatherIconMapper;

    @Inject
    AppEnvironment appEnvironment;

    @Inject
    Clock clock;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progress_bar)
    MaterialProgressBar initialLoadingProgressBar;

    @BindView(R.id.text_view_get_location_error_message)
    TextView getLocationErrorMessageTextView;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.text_view_weather_icon)
    TextView weatherIconTextView;

    @BindView(R.id.text_view_temperature_value)
    TextView currentTemperatureValueTextView;

    @BindView(R.id.text_view_weather_condition_description)
    TextView weatherDescriptionTextView;

    @BindView(R.id.text_view_time)
    TextView timeTextView;

    @BindView(R.id.text_view_max_temperature_today)
    TextView maxTemperatureTodayTextView;

    @BindView(R.id.text_view_min_temperature_today)
    TextView minTemperatureTodayTextView;

    @BindView(R.id.text_view_max_temperature_forecast)
    TextView maxTemperatureForecastTextView;

    @BindView(R.id.text_view_min_temperature_forecast)
    TextView minTemperatureForecastTextView;

    @BindView(R.id.recycler_view_daily_forecast_navigation)
    RecyclerView dailyForecastNavigationRecyclerView;

    private DailyForecastNavigationAdapter dailyForecastNavigationAdapter;

    private GoogleApiClient googleApiClient;

    private MaterialDialog permissionDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);

        // The app requires Google Play Services for using the Locations API
        if (!GoogleApiUtils.checkGoogleApiAvailability(this)) {
            Timber.w("Google Play Services not installed. Closing application now.");
            return;
        }

        ButterKnife.bind(this);

        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // reschedule location updates in case network connectivity has come back
                startLocationUpdates();
                // load current and forecast weather conditions, indicating it's a manual refresh
                presenter.loadCurrentAndForecastWeatherConditions(true);
            }
        });

        // disable swipe refresh layout by default
        swipeRefreshLayout.setEnabled(false);

        Typeface typeface = FontUtils.getWeatherIconsTypeface(this);
        weatherIconTextView.setTypeface(typeface);

        dailyForecastNavigationAdapter = new DailyForecastNavigationAdapter(
                presenter.getTemperatureUnit(),
                weatherIconMapper,
                clock,
                dailyForecastNavigationActionListener);
        dailyForecastNavigationRecyclerView.setLayoutManager(
                new LinearLayoutManager(this,  LinearLayoutManager.HORIZONTAL, false));
        dailyForecastNavigationRecyclerView.setAdapter(dailyForecastNavigationAdapter);

        // use a fixed location if it's available, otherwise request for location updates
        Coordinates fixedLocation = appEnvironment.getFixedLocation();
        if (fixedLocation != null) {
            // update location immediately
            presenter.updateCurrentLocation(fixedLocation);
        } else {
            // connect to GoogleApiClient
            if (googleApiClient == null) {
                googleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                        .enableAutoManage(this, this)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(LocationServices.API)
                        .build();
            }
        }

        // load current and forecast weather conditions
        presenter.loadCurrentAndForecastWeatherConditions(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // unregister GoogleApiClient callbacks and listener
        if (googleApiClient != null) {
            googleApiClient.unregisterConnectionCallbacks(this);
            googleApiClient.unregisterConnectionFailedListener(this);
        }

        if (permissionDialog != null) {
            permissionDialog.dismiss();
            permissionDialog = null;
        }
    }

    @Override
    protected void onStart() {
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
        super.onStart();
    }

    @Override
    protected void onStop() {
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // resume monitoring location updates
        if (googleApiClient != null && googleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // pause monitoring location updates
        if (googleApiClient != null && googleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    public HomeContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        Timber.d("onRequestPermissionsResult");
        boolean locationPermissionGranted = false;
        for (int result : grantResults) {
            if (result == PackageManager.PERMISSION_GRANTED) {
                // either fine location or coarse location access permission has been granted
                locationPermissionGranted = true;
            }
        }

        if (locationPermissionGranted && requestCode == PERMISSIONS_REQUEST_CODE) {
            if (googleApiClient != null && googleApiClient.isConnected()) {
                // reconnect GoogleApiClient
                googleApiClient.disconnect();
                googleApiClient.connect();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);

        // switch unit option based on currently selected unit
        boolean isCelsius = presenter.getTemperatureUnit() == TemperatureUnit.CELSIUS;
        menu.findItem(R.id.action_change_to_fahrenheit).setVisible(isCelsius);
        menu.findItem(R.id.action_change_to_celsius).setVisible(!isCelsius);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_change_to_fahrenheit:
                presenter.changeTemperatureUnit(TemperatureUnit.FAHRENHEIT);
                return true;
            case R.id.action_change_to_celsius:
                presenter.changeTemperatureUnit(TemperatureUnit.CELSIUS);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Timber.d("onConnected: GoogleApiClient connected.");
        // start monitoring location updates
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Timber.d("onConnectionSuspended: GoogleApiClient disconnected.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Timber.e("onConnectionFailed: " + connectionResult.getErrorMessage());

        // hide the initial loading progress spinner if currently present
        initialLoadingProgressBar.setVisibility(View.GONE);

        // enable SwipeRefreshLayout
        swipeRefreshLayout.setEnabled(true);

        // show error message in a snackbar
        Snackbar.make(
                findViewById(R.id.root_view),
                getString(R.string.error_message_could_not_resolve_location), Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void onLocationChanged(Location location) {
        Coordinates coordinates = new Coordinates(location.getLatitude(), location.getLongitude());
        presenter.updateCurrentLocation(coordinates);
    }

    /**
     * Action listener for the DailyForecastNavigationAdapter
     */
    DailyForecastNavigationAdapter.ActionListener dailyForecastNavigationActionListener = new DailyForecastNavigationAdapter.ActionListener() {
        @Override
        public void onItemClick(@NonNull SummaryForecastWeatherCondition selectedForecastWeatherCondition) {
            // select daily forecast weather condition
            presenter.selectForecastWeatherCondition(selectedForecastWeatherCondition);
        }
    };

    /**
     * Start monitoring location updates (if permission has been granted)
     */
    private void startLocationUpdates() {
        // request location updates only if GoogleApiClient is available
        if (googleApiClient == null) {
            return;
        }
        // make sure permission for accessing location is granted
        if (ActivityCompat.checkSelfPermission(HomeActivity.this, FINE_LOCATION_PERMISSION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(HomeActivity.this, COARSE_LOCATION_PERMISSION) != PackageManager.PERMISSION_GRANTED) {
            showPermissionDialog();
            return;
        }
        LocationRequest locationRequest = LocationRequest.create();

        FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, new WeakLocationListener(this));
    }

    /**
     * Stop monitoring location updates
     */
    private void stopLocationUpdates() {
        // stop location updates only if GoogleApiClient is available
        if (googleApiClient == null) {
            return;
        }
        FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }

    private void showPermissionDialog() {
        if (permissionDialog == null) {
            MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
            permissionDialog = builder.title(getString(R.string.dialog_title_access_location_permission))
                    .content(getString(R.string.dialog_content_access_location_permission))
                    .positiveText(getString(R.string.dialog_button_label_ok))
                    .negativeText(getString(R.string.dialog_button_label_cancel))
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            ActivityCompat.requestPermissions(HomeActivity.this, LOCATION_PERMISSIONS, PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .build();
        }
        permissionDialog.show();
    }

    private void showTemperatureFieldsForToday() {
        currentTemperatureValueTextView.setVisibility(View.VISIBLE);
        maxTemperatureTodayTextView.setVisibility(View.VISIBLE);
        minTemperatureTodayTextView.setVisibility(View.VISIBLE);
        maxTemperatureForecastTextView.setVisibility(View.GONE);
        minTemperatureForecastTextView.setVisibility(View.GONE);
    }

    private void showTemperatureFieldsForForecast() {
        currentTemperatureValueTextView.setVisibility(View.GONE);
        maxTemperatureTodayTextView.setVisibility(View.GONE);
        minTemperatureTodayTextView.setVisibility(View.GONE);
        maxTemperatureForecastTextView.setVisibility(View.VISIBLE);
        minTemperatureForecastTextView.setVisibility(View.VISIBLE);
    }

    /**
     * Button click listener for retrying to load weather data
     */
    View.OnClickListener onRetryButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            swipeRefreshLayout.setRefreshing(true);
            // reschedule location updates in case network connectivity has come back
            startLocationUpdates();
            presenter.loadCurrentAndForecastWeatherConditions(true);
        }
    };

    @Override
    public void showCurrentCity(@NonNull String city) {
        toolbar.setTitle(city);
        getLocationErrorMessageTextView.setVisibility(View.GONE);
    }

    @Override
    public void showCurrentWeatherCondition(@NonNull CurrentWeatherCondition currentWeatherCondition) {
        // get the currently selected temperature unit
        TemperatureUnit temperatureUnit = presenter.getTemperatureUnit();

        // display weather description
        weatherDescriptionTextView.setText(currentWeatherCondition.weatherType.getLongDescription());

        // display the current day
        DateTime dateTime = new DateTime(currentWeatherCondition.dateTimeMillis);
        String timeString = String.format(Locale.getDefault(), "%s | %s", dateTime.dayOfWeek().getAsText(), getString(R.string.today));
        timeTextView.setText(timeString);

        // display weather icon for the weather type
        int iconResId = weatherIconMapper.getWeatherIconResId(currentWeatherCondition);
        String iconString = iconResId >= 0 ? getString(iconResId) : null;
        weatherIconTextView.setText(iconString != null ? iconString : "");

        // display current temperature
        Temperature currentTemperature = temperatureUnit == TemperatureUnit.CELSIUS ? currentWeatherCondition.currentTemperature.toCelsius() : currentWeatherCondition.currentTemperature.toFahrenheit();
        currentTemperatureValueTextView.setText(currentTemperature.getFullString());

        // show the current temperature
        currentTemperatureValueTextView.setVisibility(View.VISIBLE);

        // display the forecast min and max temperature if they are available
        Temperature maxTemperature = currentWeatherCondition.forecastMaxTemperature;
        Temperature minTemperature = currentWeatherCondition.forecastMinTemperature;
        if (maxTemperature != null && minTemperature != null) {
            maxTemperature = temperatureUnit == TemperatureUnit.CELSIUS ? maxTemperature.toCelsius() : maxTemperature.toFahrenheit();
            minTemperature = temperatureUnit == TemperatureUnit.CELSIUS ? minTemperature.toCelsius() : minTemperature.toFahrenheit();
            maxTemperatureTodayTextView.setText(maxTemperature.getFullString());
            minTemperatureTodayTextView.setText(minTemperature.getFullString());
        }

        // show the current, min and max temperatures for today (current)
        showTemperatureFieldsForToday();

        // select the today's forecast weather condition
        if (dailyForecastNavigationAdapter != null) {
            dailyForecastNavigationAdapter.selectForecastWeatherConditionForToday();
        }

        // hide the refresh progress indicator if currently present
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

        // hide the initial loading progress spinner if currently present
        initialLoadingProgressBar.setVisibility(View.GONE);

        // enable SwipeRefreshLayout
        swipeRefreshLayout.setEnabled(true);
    }

    @Override
    public void showForecastWeatherCondition(@NonNull SummaryForecastWeatherCondition forecastWeatherCondition) {
        // display weather description
        weatherDescriptionTextView.setText(forecastWeatherCondition.weatherType.getLongDescription());

        // display the day of the forecast
        DateTime dateTime = new DateTime(forecastWeatherCondition.dateTimeMillis);
        timeTextView.setText(dateTime.dayOfWeek().getAsText());

        // display weather icon for the weather type
        int iconResId = weatherIconMapper.getWeatherIconResId(forecastWeatherCondition);
        String iconString = iconResId >= 0 ? getString(iconResId) : null;
        weatherIconTextView.setText(iconString != null ? iconString : "");

        // hide the current temperature
        currentTemperatureValueTextView.setVisibility(View.GONE);

        // show the min and max temperatures for forecast
        showTemperatureFieldsForForecast();
        // display max and min temperatures for forecast
        TemperatureUnit temperatureUnit = presenter.getTemperatureUnit();
        Temperature maxTemperature = temperatureUnit == TemperatureUnit.CELSIUS ? forecastWeatherCondition.forecastMaxTemperature.toCelsius() : forecastWeatherCondition.forecastMaxTemperature.toFahrenheit();
        Temperature minTemperature = temperatureUnit == TemperatureUnit.CELSIUS ? forecastWeatherCondition.forecastMinTemperature.toCelsius() : forecastWeatherCondition.forecastMinTemperature.toFahrenheit();
        maxTemperatureForecastTextView.setText(maxTemperature.getFullString());
        minTemperatureForecastTextView.setText(minTemperature.getFullString());

        // select forecast weather condition
        if (dailyForecastNavigationAdapter != null) {
            dailyForecastNavigationAdapter.selectForecastWeatherCondition(forecastWeatherCondition);
        }

        // hide the refresh progress indicator if currently present
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

        // hide the initial loading progress spinner if currently present
        initialLoadingProgressBar.setVisibility(View.GONE);

        // enable SwipeRefreshLayout
        swipeRefreshLayout.setEnabled(true);
    }

    @Override
    public void showDailyForecastWeatherConditionsList(@NonNull List<SummaryForecastWeatherCondition> dailyForecastWeatherConditions) {
        if (dailyForecastNavigationAdapter != null && !dailyForecastWeatherConditions.isEmpty()) {
            dailyForecastNavigationAdapter.setDailyForecastWeatherConditions(dailyForecastWeatherConditions);

            // hide the initial loading progress spinner if currently present
            initialLoadingProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showInitialLoadingView() {
        if (!swipeRefreshLayout.isEnabled()) {
            initialLoadingProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showCannotResolveLocationError() {
        // hide the refresh progress indicator if currently present
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

        // hide the initial loading progress spinner if currently present
        initialLoadingProgressBar.setVisibility(View.GONE);

        // show the main error message if no location (city name) has been resolved
        if (toolbar.getTitle().length() == 0) {
            getLocationErrorMessageTextView.setVisibility(View.VISIBLE);
        }

        // show error message in a snackbar
        Snackbar.make(
                findViewById(R.id.root_view),
                getString(R.string.error_message_could_not_resolve_location), Snackbar.LENGTH_LONG)
                .show();

        // enable SwipeRefreshLayout
        swipeRefreshLayout.setEnabled(true);
    }

    @Override
    public void showCannotLoadWeatherDataError() {
        // hide the refresh progress indicator if currently present
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

        // hide the initial loading progress spinner if currently present
        initialLoadingProgressBar.setVisibility(View.GONE);

        // show error message in a snackbar with a retry button
        Snackbar.make(
                findViewById(R.id.root_view),
                getString(R.string.error_message_could_not_load_weather_data), Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.button_text_retry, onRetryButtonClickListener)
                .show();

        // disable SwipeRefreshLayout
        swipeRefreshLayout.setEnabled(false);
    }

    @Override
    public void refreshView() {
        // recreate activity
        recreate();
    }
}
