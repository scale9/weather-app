package io.ychescale9.weatherapp.presentation.home;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.ychescale9.weatherapp.R;
import io.ychescale9.weatherapp.data.model.SummaryForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.Temperature;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;
import io.ychescale9.weatherapp.presentation.helper.SummaryForecastWeatherConditionDiffCallback;
import io.ychescale9.weatherapp.presentation.helper.WeatherIconMapper;
import io.ychescale9.weatherapp.presentation.util.FontUtils;
import io.ychescale9.weatherapp.util.Clock;
import io.ychescale9.weatherapp.util.DateUtils;

import static io.ychescale9.weatherapp.R.string.today;

/**
 * Created by yang on 9/12/16.
 */
class DailyForecastNavigationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    interface ActionListener {
        void onItemClick(@NonNull SummaryForecastWeatherCondition selectedForecastWeatherCondition);
    }

    private final WeatherIconMapper weatherIconMapper;
    private final ActionListener actionListener;
    private final Clock clock;

    private TemperatureUnit temperatureUnit;
    private List<SummaryForecastWeatherCondition> dailyForecastWeatherConditions;
    private SummaryForecastWeatherCondition selectedForecastWeatherCondition;

    DailyForecastNavigationAdapter(@NonNull TemperatureUnit temperatureUnit,
                                   @NonNull WeatherIconMapper weatherIconMapper,
                                   @NonNull Clock clock,
                                   @NonNull ActionListener actionListener) {
        this.temperatureUnit = temperatureUnit;
        this.weatherIconMapper = weatherIconMapper;
        this.clock = clock;
        this.actionListener = actionListener;
        this.dailyForecastWeatherConditions = new ArrayList<>();
    }

    public void setTemperatureUnit(TemperatureUnit temperatureUnit) {
        if (this.temperatureUnit != temperatureUnit) {
            this.temperatureUnit = temperatureUnit;
            notifyDataSetChanged();
        }
    }

    void setDailyForecastWeatherConditions(@NonNull List<SummaryForecastWeatherCondition> dailyForecastWeatherConditions) {
        dispatchDiffResult(this.dailyForecastWeatherConditions, dailyForecastWeatherConditions);
        this.dailyForecastWeatherConditions = dailyForecastWeatherConditions;
    }

    void selectForecastWeatherCondition(@NonNull SummaryForecastWeatherCondition dailyForecastWeatherCondition) {
        if (!dailyForecastWeatherCondition.equals(selectedForecastWeatherCondition)) {
            selectedForecastWeatherCondition = dailyForecastWeatherCondition;
            notifyDataSetChanged();
        }
    }

    void selectForecastWeatherConditionForToday() {
        // try to find today's forecast weather condition
        for (SummaryForecastWeatherCondition weatherCondition : dailyForecastWeatherConditions) {
            if (DateUtils.isSameDay(weatherCondition.dateTimeMillis, clock.getCurrentTimeMillis())) {
                if (!weatherCondition.equals(selectedForecastWeatherCondition)) {
                    selectedForecastWeatherCondition = weatherCondition;
                    notifyDataSetChanged();
                }
                break;
            }
        }
    }

    private void dispatchDiffResult(List<SummaryForecastWeatherCondition> newWeatherConditions,
                                    List<SummaryForecastWeatherCondition> oldWeatherConditions) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(
                new SummaryForecastWeatherConditionDiffCallback(newWeatherConditions, oldWeatherConditions));
        diffResult.dispatchUpdatesTo(this);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View rowView;
        rowView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_daily_forecast_navigation, viewGroup, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int i) {
        ViewHolder vh = (ViewHolder) viewHolder;
        final SummaryForecastWeatherCondition weatherCondition = dailyForecastWeatherConditions.get(i);

        // set day of the week
        if (DateUtils.isSameDay(weatherCondition.dateTimeMillis, clock.getCurrentTimeMillis())) {
            // display "Today" if datetime is within the interval of today
            vh.mDayOfWeekTextView.setText(vh.mRootView.getContext().getString(today));
        } else {
            // display short string representation of the day of the week
            DateTime dateTime = new DateTime(weatherCondition.dateTimeMillis);
            vh.mDayOfWeekTextView.setText(dateTime.dayOfWeek().getAsShortText());
        }

        // set weather icon
        int iconResId = weatherIconMapper.getWeatherIconResId(weatherCondition);
        String iconString = iconResId >= 0 ? vh.mRootView.getContext().getString(iconResId) : null;
        vh.mWeatherIconTextView.setText(iconString != null ? iconString : "");

        // set temperate range
        Temperature maxTemperature = temperatureUnit == TemperatureUnit.CELSIUS ? weatherCondition.forecastMaxTemperature.toCelsius() : weatherCondition.forecastMaxTemperature.toFahrenheit();
        Temperature minTemperature = temperatureUnit == TemperatureUnit.CELSIUS ? weatherCondition.forecastMinTemperature.toCelsius() : weatherCondition.forecastMinTemperature.toFahrenheit();
        String temperatureRangeString = String.format(
                Locale.getDefault(), "%s/%s",
                maxTemperature.getShortString(),
                minTemperature.getShortString()
        );
        vh.mTemperatureRangeTextView.setText(temperatureRangeString);

        // set background color
        if (selectedForecastWeatherCondition != null && selectedForecastWeatherCondition.equals(weatherCondition)) {
            vh.mRootView.setBackgroundColor(ContextCompat.getColor(vh.mRootView.getContext(), R.color.colorPrimary));
        } else {
            vh.mRootView.setBackgroundColor(ContextCompat.getColor(vh.mRootView.getContext(), android.R.color.transparent));
        }

        // set on click listener for item
        vh.mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!weatherCondition.equals(selectedForecastWeatherCondition)) {
                    selectedForecastWeatherCondition = weatherCondition;
                    actionListener.onItemClick(weatherCondition);
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dailyForecastWeatherConditions.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.root_view) FrameLayout mRootView;
        @BindView(R.id.text_view_day) TextView mDayOfWeekTextView;
        @BindView(R.id.text_view_weather_icon) TextView mWeatherIconTextView;
        @BindView(R.id.text_view_temperature_range) TextView mTemperatureRangeTextView;

        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            Typeface typeface = FontUtils.getWeatherIconsTypeface(mRootView.getContext());
            mWeatherIconTextView.setTypeface(typeface);
        }
    }
}
