package io.ychescale9.weatherapp.presentation.helper;

import android.support.annotation.NonNull;

import io.ychescale9.weatherapp.R;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.WeatherCondition;
import io.ychescale9.weatherapp.util.Clock;

/**
 * Created by yang on 11/12/16.
 */
public class WeatherIconMapper {

    private final Clock mClock;

    public WeatherIconMapper(Clock clock) {
        mClock = clock;
    }

    /**
     * Maps the weatherId against corresponding weather icon
     * @param weatherCondition - resource if of the string representation of the icon
     */
    public int getWeatherIconResId(@NonNull WeatherCondition weatherCondition) {
        int weatherId = weatherCondition.weatherType.getWeatherId();
        // For weather condition codes and groups see https://openweathermap.org/weather-conditions
        int weatherGroup = weatherId / 100;
        int iconResId = -1;

        if (weatherId == 800) {
            // clear sky
            if (weatherCondition instanceof CurrentWeatherCondition) {
                // current weather condition includes sunrise and sunset timestamps
                // which can be used to choose day / night icon
                CurrentWeatherCondition currentWeatherCondition = (CurrentWeatherCondition) weatherCondition;
                long currentTimeMillis = mClock.getCurrentTimeMillis();
                if (currentTimeMillis >= currentWeatherCondition.sunriseDateTimeMillis && currentTimeMillis < currentWeatherCondition.sunsetDateTimeMillis) {
                    iconResId = R.string.weather_clear_day;
                } else {
                    iconResId = R.string.weather_clear_night;
                }
            } else {
                // use the day icon for forecast weather conditions
                iconResId = R.string.weather_clear_day;
            }
        } else {
            switch(weatherGroup) {
                case 2:
                    // Thunderstorm
                    iconResId = R.string.weather_thunder;
                    break;
                case 3:
                    // Drizzle
                    iconResId = R.string.weather_drizzle;
                    break;
                case 5:
                    // Rain
                    iconResId = R.string.weather_rainy;
                    break;
                case 6:
                    // Snow
                    iconResId = R.string.weather_snowy;
                    break;
                case 7:
                    // Atmosphere
                    iconResId = R.string.weather_foggy;
                    break;
                case 8:
                    // cloudy
                    iconResId = R.string.weather_cloudy;
                    break;
            }
        }
        return iconResId;
    }
}
