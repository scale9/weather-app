package io.ychescale9.weatherapp.presentation.home;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.ychescale9.weatherapp.data.disk.SharedPreferencesManager;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.SimpleAddress;
import io.ychescale9.weatherapp.data.model.SummaryForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;
import io.ychescale9.weatherapp.domain.LoadCurrentWeatherCondition;
import io.ychescale9.weatherapp.domain.LoadDailyForecastWeatherConditions;
import io.ychescale9.weatherapp.domain.ReverseGeocode;
import io.ychescale9.weatherapp.util.Clock;
import io.ychescale9.weatherapp.util.DateUtils;
import io.ychescale9.weatherapp.util.DefaultObserver;
import timber.log.Timber;

/**
 * Created by yang on 9/12/16.
 */
class HomePresenter implements HomeContract.Presenter {

    private HomeContract.View view;

    @NonNull
    private final SharedPreferencesManager sharedPreferencesManager;

    @NonNull
    private final Clock clock;

    @NonNull
    private final ReverseGeocode reverseGeocode;

    @NonNull
    private final LoadCurrentWeatherCondition loadCurrentWeatherCondition;

    @NonNull
    private final LoadDailyForecastWeatherConditions loadDailyForecastWeatherConditions;

    private final int autoRefreshPollingIntervalMilliseconds;
    private final int forecastDataUpdateIntervalMilliseconds;

    @NonNull
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    private Disposable periodicCurrentWeatherRefreshDisposable;
    private Disposable periodicForecastRefreshDisposable;

    private Coordinates currentCoordinates;

    private String currentCity;

    private CurrentWeatherCondition currentWeatherCondition;

    private final List<SummaryForecastWeatherCondition> summaryForecastWeatherConditions = new ArrayList<>();

    // the currently selected forecast weather condition (this might be "today")
    private SummaryForecastWeatherCondition selectedForecastWeatherCondition;

    HomePresenter(@NonNull HomeContract.View view,
                  @NonNull SharedPreferencesManager sharedPreferencesManager,
                  @NonNull Clock clock,
                  @NonNull ReverseGeocode reverseGeocode,
                  @NonNull LoadCurrentWeatherCondition loadCurrentWeatherCondition,
                  @NonNull LoadDailyForecastWeatherConditions loadDailyForecastWeatherConditions,
                  int autoRefreshPollingIntervalMilliseconds,
                  int forecastDataUpdateIntervalMilliseconds) {
        this.view = view;
        this.sharedPreferencesManager = sharedPreferencesManager;
        this.clock = clock;
        this.reverseGeocode = reverseGeocode;
        this.loadCurrentWeatherCondition = loadCurrentWeatherCondition;
        this.loadDailyForecastWeatherConditions = loadDailyForecastWeatherConditions;
        this.autoRefreshPollingIntervalMilliseconds = autoRefreshPollingIntervalMilliseconds;
        this.forecastDataUpdateIntervalMilliseconds = forecastDataUpdateIntervalMilliseconds;
    }

    @Override
    public void loadCurrentAndForecastWeatherConditions(boolean manualRefresh) {
        if (currentWeatherCondition == null && summaryForecastWeatherConditions.isEmpty()) {
            view.showInitialLoadingView();
        }
        if (currentCoordinates != null) {
            if (currentCity != null) {
                // display city in the view
                view.showCurrentCity(currentCity);
            }

            // load current weather condition
            loadCurrentWeatherCondition.setCoordinates(currentCoordinates);
            loadCurrentWeatherCondition.setInvalidateCache(manualRefresh); // invalidate cache if manually refreshing
            compositeDisposable.add(
                    loadCurrentWeatherCondition.getStream()
                            .subscribeWith(new LoadCurrentWeatherConditionObserver())
            );

            // load daily forecast weather conditions
            loadDailyForecastWeatherConditions.setCoordinates(currentCoordinates);
            loadDailyForecastWeatherConditions.setInvalidateCache(manualRefresh); // invalidate cache if manually refreshing
            compositeDisposable.add(
                    loadDailyForecastWeatherConditions.getStream()
                            .subscribeWith(new LoadDailyForecastWeatherConditionsObserver())
            );

            // start periodic data refresh
            startPeriodicCurrentWeatherRefresh();
            startPeriodicForecastRefresh();
        }
    }

    @Override
    public void updateCurrentLocation(@NonNull Coordinates coordinates) {
        // only update coordinates if changed
        if (currentCoordinates == null || !currentCoordinates.equals(coordinates)) {
            currentCoordinates = coordinates;
            // update coordinates for repeating use cases
            loadCurrentWeatherCondition.setCoordinates(currentCoordinates);
            loadDailyForecastWeatherConditions.setCoordinates(currentCoordinates);
            // reverse geocode the coordinates to get the address
            reverseGeocode.setCoordinates(coordinates);
            compositeDisposable.add(
                    reverseGeocode.getStream()
                    .subscribeWith(new DisposableObserver<SimpleAddress>() {
                        @Override
                        public void onNext(SimpleAddress address) {
                            // only reload the weather conditions if the actual city has changed
                            if (!address.getLocality().equals(currentCity)) {
                                Timber.d("Current city has changed, reloading current and forecast weather conditions.");
                                // update the current city
                                currentCity = address.getLocality();

                                // display city in the view
                                view.showCurrentCity(currentCity);
                                // reload current and forecast weather conditions
                                // force refresh as location has changed
                                loadCurrentAndForecastWeatherConditions(true);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Timber.e("Error trying to reverse geocode the current coordinates.");
                            currentCoordinates = null;
                            view.showCannotResolveLocationError();
                        }

                        @Override
                        public void onComplete() {

                        }
                    })
            );
        }
    }

    @Override
    public void selectForecastWeatherCondition(@NonNull SummaryForecastWeatherCondition forecastWeatherCondition) {
        if (selectedForecastWeatherCondition != forecastWeatherCondition) {
            selectedForecastWeatherCondition = forecastWeatherCondition;
            // refresh view
            updateViewForecastWeatherConditionSelection();
        }
    }

    @Override
    public void changeTemperatureUnit(@NonNull TemperatureUnit unit) {
        sharedPreferencesManager.setTemperatureUnit(unit.getUnitName());
        view.refreshView();
    }

    @Override
    public TemperatureUnit getTemperatureUnit() {
        // Default unit is Celsius
        return TemperatureUnit.fromUnitName(sharedPreferencesManager.getTemperatureUnit(TemperatureUnit.CELSIUS.getUnitName()));
    }

    @Override
    public void setView(@Nullable HomeContract.View view) {
        this.view = view;
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
        compositeDisposable.clear();
        stopPeriodicCurrentWeatherRefresh();
        stopPeriodicForecastRefresh();
        view = null;
    }

    /**
     * Start periodically loading current weather data
     */
    @SuppressWarnings("unchecked")
    private void startPeriodicCurrentWeatherRefresh() {
        stopPeriodicForecastRefresh();
        periodicForecastRefreshDisposable = loadDailyForecastWeatherConditions.getStream()
                .repeatWhen(new Function<Observable<Object>, ObservableSource<?>>() {
                    @Override
                    public ObservableSource<?> apply(Observable<Object> objectObservable) throws Exception {
                        return objectObservable.delay(forecastDataUpdateIntervalMilliseconds, TimeUnit.MILLISECONDS);
                    }
                })
                .delaySubscription(forecastDataUpdateIntervalMilliseconds, TimeUnit.MILLISECONDS)
                .subscribeWith(new LoadDailyForecastWeatherConditionsObserver());
    }

    private void stopPeriodicForecastRefresh() {
        if (periodicForecastRefreshDisposable != null && !periodicForecastRefreshDisposable.isDisposed()) {
            periodicForecastRefreshDisposable.dispose();
            periodicForecastRefreshDisposable = null;
        }
    }

    /**
     * Start periodically loading forecast data
     */
    @SuppressWarnings("unchecked")
    private void startPeriodicForecastRefresh() {
        stopPeriodicCurrentWeatherRefresh();
        periodicCurrentWeatherRefreshDisposable = loadCurrentWeatherCondition.getStream()
                .repeatWhen(new Function<Observable<Object>, ObservableSource<?>>() {
                    @Override
                    public ObservableSource<?> apply(Observable<Object> objectObservable) throws Exception {
                        return objectObservable.delay(autoRefreshPollingIntervalMilliseconds, TimeUnit.MILLISECONDS);
                    }
                })
                .delaySubscription(autoRefreshPollingIntervalMilliseconds, TimeUnit.MILLISECONDS)
                .subscribeWith(new LoadCurrentWeatherConditionObserver());
    }

    private void stopPeriodicCurrentWeatherRefresh() {
        if (periodicCurrentWeatherRefreshDisposable != null && !periodicCurrentWeatherRefreshDisposable.isDisposed()) {
            periodicCurrentWeatherRefreshDisposable.dispose();
            periodicCurrentWeatherRefreshDisposable = null;
        }
    }

    /**
     * Refresh the view based on the currently selected forecast weather condition
     */
    private void updateViewForecastWeatherConditionSelection() {
        if (selectedForecastWeatherCondition == null) {
            return;
        }
        if (currentWeatherCondition != null && DateUtils.isSameDay(selectedForecastWeatherCondition.dateTimeMillis, clock.getCurrentTimeMillis())) {
            // selected forecast weather condition is for today, refresh the view with the current weather condition
            view.showCurrentWeatherCondition(currentWeatherCondition);
        } else {
            // update the view with the selected forecast weather condition
            view.showForecastWeatherCondition(selectedForecastWeatherCondition);
        }
    }

    private final class LoadCurrentWeatherConditionObserver extends DefaultObserver<CurrentWeatherCondition> {
        @Override
        public void onNext(CurrentWeatherCondition currentWeatherCondition) {
            HomePresenter.this.currentWeatherCondition = currentWeatherCondition;

            // update the currentWeatherCondition with today's forecast max and min temperatures if available
            for (SummaryForecastWeatherCondition weatherCondition : summaryForecastWeatherConditions) {
                if (DateUtils.isSameDay(weatherCondition.dateTimeMillis, clock.getCurrentTimeMillis())) {
                    currentWeatherCondition.forecastMaxTemperature = weatherCondition.forecastMaxTemperature;
                    currentWeatherCondition.forecastMinTemperature = weatherCondition.forecastMinTemperature;
                    break;
                }
            }

            // if no forecast weather condition has been selected or the selected forecast weather condition is for Today,
            // update view with the loaded current weather condition
            if (selectedForecastWeatherCondition == null || DateUtils.isSameDay(
                    selectedForecastWeatherCondition.dateTimeMillis, clock.getCurrentTimeMillis())) {
                view.showCurrentWeatherCondition(currentWeatherCondition);
            }
        }

        @Override
        public void onError(Throwable e) {
            Timber.w("Error loading current weather condition.");
            view.showCannotLoadWeatherDataError();

            // stop periodic data refresh
            stopPeriodicCurrentWeatherRefresh();
            stopPeriodicForecastRefresh();
        }
    }

    private final class LoadDailyForecastWeatherConditionsObserver extends DefaultObserver<List<SummaryForecastWeatherCondition>> {
        @Override
        public void onNext(List<SummaryForecastWeatherCondition> dailyForecastWeatherConditions) {
            // update the list of loaded forecast weather conditions in the view
            summaryForecastWeatherConditions.clear();
            summaryForecastWeatherConditions.addAll(dailyForecastWeatherConditions);
            view.showDailyForecastWeatherConditionsList(summaryForecastWeatherConditions);

            // find the forecast max and min temperatures for Today and update the CurrentWeatherCondition
            // also update the selectedForecastWeatherCondition if a SummaryForecastWeatherCondition matching it can be found in the new list loaded
            boolean foundMatchingForecastWeatherCondition = false;
            for (SummaryForecastWeatherCondition weatherCondition : summaryForecastWeatherConditions) {
                // update the currentWeatherCondition with today's forecast max and min temperatures
                if (currentWeatherCondition != null && DateUtils.isSameDay(weatherCondition.dateTimeMillis, clock.getCurrentTimeMillis())) {
                    currentWeatherCondition.forecastMaxTemperature = weatherCondition.forecastMaxTemperature;
                    currentWeatherCondition.forecastMinTemperature = weatherCondition.forecastMinTemperature;
                }

                // update selectedForecastWeatherCondition if found in the list (by comparing dateTime)
                if (selectedForecastWeatherCondition != null &&
                        selectedForecastWeatherCondition.dateTimeMillis == weatherCondition.dateTimeMillis) {
                    selectedForecastWeatherCondition = weatherCondition;
                    foundMatchingForecastWeatherCondition = true;
                }
            }

            // select the first forecast weather condition if
            // either previously selected forecast weather condition is no longer relevant,
            // or none has been selected before
            if (!foundMatchingForecastWeatherCondition || selectedForecastWeatherCondition == null) {
                selectedForecastWeatherCondition = summaryForecastWeatherConditions.get(0);
            }

            // refresh view
            updateViewForecastWeatherConditionSelection();
        }

        @Override
        public void onError(Throwable e) {
            Timber.w("Error loading daily forecast weather conditions.");
            view.showCannotLoadWeatherDataError();

            // stop periodic data refresh
            stopPeriodicCurrentWeatherRefresh();
            stopPeriodicForecastRefresh();
        }
    }
}
