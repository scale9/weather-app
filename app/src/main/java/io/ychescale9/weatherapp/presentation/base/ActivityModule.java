package io.ychescale9.weatherapp.presentation.base;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import io.ychescale9.weatherapp.presentation.home.HomeActivity;
import io.ychescale9.weatherapp.presentation.home.HomeModule;

/**
 * Created by yang on 29/4/17.
 */
@Module
public abstract class ActivityModule {

    @ViewScope
    @ContributesAndroidInjector(modules = HomeModule.class)
    abstract HomeActivity homeActivity();
}
