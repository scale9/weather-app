package io.ychescale9.weatherapp.presentation.helper;

import android.support.v7.util.DiffUtil;

import java.util.List;

import io.ychescale9.weatherapp.data.model.SummaryForecastWeatherCondition;

/**
 * Created by yang on 13/12/16.
 */
public class SummaryForecastWeatherConditionDiffCallback extends DiffUtil.Callback {

    private List<SummaryForecastWeatherCondition> oldWeatherConditions;
    private List<SummaryForecastWeatherCondition> newWeatherConditions;

    public SummaryForecastWeatherConditionDiffCallback(List<SummaryForecastWeatherCondition> newWeatherConditions,
                                                       List<SummaryForecastWeatherCondition> oldWeatherConditions) {
        this.newWeatherConditions = newWeatherConditions;
        this.oldWeatherConditions = oldWeatherConditions;
    }

    @Override
    public int getOldListSize() {
        return oldWeatherConditions.size();
    }

    @Override
    public int getNewListSize() {
        return newWeatherConditions.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldWeatherConditions.get(oldItemPosition).dateTimeMillis == newWeatherConditions.get(newItemPosition).dateTimeMillis;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldWeatherConditions.get(oldItemPosition).equals(newWeatherConditions.get(newItemPosition));
    }
}
