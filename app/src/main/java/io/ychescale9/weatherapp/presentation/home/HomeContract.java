package io.ychescale9.weatherapp.presentation.home;

import android.support.annotation.NonNull;

import java.util.List;

import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.SummaryForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;
import io.ychescale9.weatherapp.presentation.base.IPresenter;

/**
 * Created by yang on 9/12/16.
 */
public interface HomeContract {

    interface View {

        void showCurrentCity(@NonNull String city);

        void showCurrentWeatherCondition(@NonNull CurrentWeatherCondition currentWeatherCondition);

        void showForecastWeatherCondition(@NonNull SummaryForecastWeatherCondition forecastWeatherCondition);

        void showDailyForecastWeatherConditionsList(@NonNull List<SummaryForecastWeatherCondition> dailyForecastWeatherConditions);

        void showInitialLoadingView();

        void showCannotResolveLocationError();

        void showCannotLoadWeatherDataError();

        void refreshView();
    }

    interface Presenter extends IPresenter<View> {

        void loadCurrentAndForecastWeatherConditions(boolean manualRefresh);

        void updateCurrentLocation(@NonNull Coordinates coordinates);

        void selectForecastWeatherCondition(@NonNull SummaryForecastWeatherCondition forecastWeatherCondition);

        void changeTemperatureUnit(@NonNull TemperatureUnit unit);

        TemperatureUnit getTemperatureUnit();
    }
}
