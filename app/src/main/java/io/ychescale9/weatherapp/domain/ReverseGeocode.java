package io.ychescale9.weatherapp.domain;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.SimpleAddress;
import io.ychescale9.weatherapp.util.GeocodingHelper;
import io.ychescale9.weatherapp.util.SchedulerProvider;
import timber.log.Timber;

/**
 * Created by yang on 10/12/16.
 * Converts coordinates to an address
 */
public class ReverseGeocode extends UseCase<SimpleAddress> {

    // find a single address
    private static final int MAX_RESULTS = 1;

    private final GeocodingHelper geocodingHelper;

    private Coordinates coordinates;

    public ReverseGeocode(SchedulerProvider schedulerProvider, GeocodingHelper geocodingHelper) {
        super(schedulerProvider);
        this.geocodingHelper = geocodingHelper;
    }

    public void setCoordinates(@NonNull Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    protected Observable<SimpleAddress> createUseCase() {
        return Observable.fromCallable(new Callable<SimpleAddress>() {
            @Override
            public SimpleAddress call() throws Exception {
                if (coordinates == null) {
                    throw new Exception("Can't execute: no coordinates provided.");
                }

                double latitude = coordinates.getLatitude();
                double longitude = coordinates.getLongitude();

                List<SimpleAddress> addresses;

                try {
                    addresses = geocodingHelper.getFromLocation(latitude, longitude, MAX_RESULTS);
                } catch (IOException ioException) {
                    Timber.e(ioException, "Network or other I/O issues found.");
                    throw ioException;
                } catch (IllegalArgumentException illegalArgumentException) {
                    Timber.e(illegalArgumentException, "Invalid latitude or longitude values provided.");
                    throw illegalArgumentException;
                }

                // check if at least 1 address is found
                if (addresses == null || addresses.size() == 0) {
                    Timber.e("No address found for coordinates " + latitude + "," + longitude);
                    throw new Exception("No address found at the location " + latitude+ "," + longitude + ".");
                }

                return addresses.get(0);
            }
        });
    }
}
