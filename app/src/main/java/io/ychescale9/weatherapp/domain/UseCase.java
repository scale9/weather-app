package io.ychescale9.weatherapp.domain;

import io.reactivex.Observable;
import io.ychescale9.weatherapp.util.SchedulerProvider;

/**
 * Created by yang on 9/12/16.
 */
public abstract class UseCase<T> {

    private final SchedulerProvider schedulerProvider;

    UseCase(SchedulerProvider schedulerProvider) {
        this.schedulerProvider = schedulerProvider;
    }

    protected abstract Observable<T> createUseCase();

    public Observable<T> getStream() {
        return createUseCase().compose(schedulerProvider.<T>applySchedulers());
    }
}