package io.ychescale9.weatherapp.domain;

import android.support.annotation.NonNull;

import io.reactivex.Observable;
import io.ychescale9.weatherapp.data.cache.CurrentWeatherConditionCache;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.repository.WeatherAppRepository;
import io.ychescale9.weatherapp.util.SchedulerProvider;

/**
 * Created by yang on 10/12/16.
 */
public class LoadCurrentWeatherCondition extends UseCase<CurrentWeatherCondition> {

    private final WeatherAppRepository weatherAppRepository;
    private final CurrentWeatherConditionCache currentWeatherConditionCache;

    private Coordinates coordinates;

    // this will implicitly force the repository to load data from the server
    // could be useful for manual data refresh
    private boolean invalidateCache = false;

    public LoadCurrentWeatherCondition(SchedulerProvider schedulerProvider,
                                       WeatherAppRepository weatherAppRepository,
                                       CurrentWeatherConditionCache currentWeatherConditionCache) {
        super(schedulerProvider);
        this.weatherAppRepository = weatherAppRepository;
        this.currentWeatherConditionCache = currentWeatherConditionCache;
    }

    public void setCoordinates(@NonNull Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public void setInvalidateCache(boolean invalidateCache) {
        this.invalidateCache = invalidateCache;
    }

    @Override
    protected Observable<CurrentWeatherCondition> createUseCase() {
        if (coordinates == null) {
            return Observable.error(new Exception("Can't execute: no coordinates provided."));
        }
        // clearing the cache will implicitly force the repository to load data from server
        if (invalidateCache) {
            currentWeatherConditionCache.clear();
        }
        return weatherAppRepository.loadCurrentWeatherCondition(coordinates);
    }
}
