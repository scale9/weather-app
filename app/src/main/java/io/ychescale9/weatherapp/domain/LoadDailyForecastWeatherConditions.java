package io.ychescale9.weatherapp.domain;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Observable;
import io.ychescale9.weatherapp.data.cache.DailyForecastWeatherConditionsCache;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.SummaryForecastWeatherCondition;
import io.ychescale9.weatherapp.data.repository.WeatherAppRepository;
import io.ychescale9.weatherapp.util.SchedulerProvider;

/**
 * Created by yang on 11/12/16.
 */
public class LoadDailyForecastWeatherConditions extends UseCase<List<SummaryForecastWeatherCondition>> {

    private final WeatherAppRepository weatherAppRepository;
    private final DailyForecastWeatherConditionsCache dailyForecastWeatherConditionsCache;

    private Coordinates coordinates;

    // this will implicitly force the repository to load data from the server
    // could be useful for manual data refresh
    private boolean invalidateCache = false;

    public LoadDailyForecastWeatherConditions(SchedulerProvider schedulerProvider,
                                              WeatherAppRepository weatherAppRepository,
                                              DailyForecastWeatherConditionsCache dailyForecastWeatherConditionsCache) {
        super(schedulerProvider);
        this.weatherAppRepository = weatherAppRepository;
        this.dailyForecastWeatherConditionsCache = dailyForecastWeatherConditionsCache;
    }

    public void setCoordinates(@NonNull Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public void setInvalidateCache(boolean invalidateCache) {
        this.invalidateCache = invalidateCache;
    }

    @Override
    protected Observable<List<SummaryForecastWeatherCondition>> createUseCase() {
        if (coordinates == null) {
            return Observable.error(new Exception("Can't execute: no coordinates provided."));
        }
        // clearing the cache will implicitly force the repository to load data from server
        if (invalidateCache) {
            dailyForecastWeatherConditionsCache.clear();
        }
        return weatherAppRepository.loadDailyForecastWeatherConditions(coordinates);
    }
}
