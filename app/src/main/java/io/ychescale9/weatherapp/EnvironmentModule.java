package io.ychescale9.weatherapp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by yang on 16/12/16.
 */
@Module
public class EnvironmentModule {

    @Provides
    @Singleton
    AppEnvironment provideAppEnvironment() {
        return new AppEnvironment();
    }
}
