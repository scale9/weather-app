package io.ychescale9.weatherapp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.weatherapp.util.SchedulerProvider;

/**
 * Created by yang on 9/12/16.
 */
@Module
public class AppModule {

    @Provides
    @Singleton
    SchedulerProvider provideSchedulerProvider() {
        return new SchedulerProvider();
    }
}
