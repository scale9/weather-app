package io.ychescale9.weatherapp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.weatherapp.data.api.ApiConstants;

/**
 * Created by yang on 9/12/16.
 */
@Module
public class ConstantsModule {

    @Provides
    @Singleton
    ApiConstants provideApiConstants() {
        return new ApiConstants();
    }
}