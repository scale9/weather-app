package io.ychescale9.weatherapp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.weatherapp.util.Clock;
import io.ychescale9.weatherapp.util.RealClock;

/**
 * Created by yang on 16/12/16.
 */
@Module
public class TimeModule {

    @Provides
    @Singleton
    Clock provideClock() {
        return new RealClock();
    }
}
