package io.ychescale9.weatherapp;

import io.ychescale9.weatherapp.data.model.Coordinates;

/**
 * Created by yang on 15/12/16.
 */
public abstract class BaseAppEnvironment {

    /**
     * Returns a fixed location which will be used by the app
     * @return
     */
    public abstract Coordinates getFixedLocation();
}
