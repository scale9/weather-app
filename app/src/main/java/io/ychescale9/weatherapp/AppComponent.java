package io.ychescale9.weatherapp;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import io.ychescale9.weatherapp.data.DataModule;
import io.ychescale9.weatherapp.data.api.ApiModule;
import io.ychescale9.weatherapp.data.cache.CacheModule;
import io.ychescale9.weatherapp.data.repository.RepositoryModule;
import io.ychescale9.weatherapp.presentation.base.ActivityModule;

/**
 * Created by yang on 9/12/16.
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
                EnvironmentModule.class,
                ConstantsModule.class,
                TimeModule.class,
                DataModule.class,
                RepositoryModule.class,
                ApiModule.class,
                CacheModule.class,
                ActivityModule.class, // all activity sub-components
        }
)
interface AppComponent {

    void inject(BaseWeatherAppApplication application);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder app(Application application);

        AppComponent build();
    }
}
