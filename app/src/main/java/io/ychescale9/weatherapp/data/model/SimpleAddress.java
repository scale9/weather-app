package io.ychescale9.weatherapp.data.model;

import java.util.Locale;

/**
 * Created by yang on 29/4/17.
 * Simple version of the android.location.Address with for mocking in unit testing
 */
public class SimpleAddress {

    private final Locale locale;
    private final String featureName;
    private final String adminArea;
    private final String subAdminArea;
    private final String locality;
    private final String subLocality;
    private final String thoroughfare;
    private final String subThoroughfare;
    private final String premises;
    private final String postalCode;
    private final String countryCode;
    private final String countryName;
    private final double latitude;
    private final double longitude;
    private final String phone;
    private final String url;

    public SimpleAddress(Locale locale,
                         String featureName,
                         String adminArea,
                         String subAdminArea,
                         String locality,
                         String subLocality,
                         String thoroughfare,
                         String subThoroughfare,
                         String premises,
                         String postalCode,
                         String countryCode,
                         String countryName,
                         double latitude,
                         double longitude,
                         String phone,
                         String url) {
        this.locale = locale;
        this.featureName = featureName;
        this.adminArea = adminArea;
        this.subAdminArea = subAdminArea;
        this.locality = locality;
        this.subLocality = subLocality;
        this.thoroughfare = thoroughfare;
        this.subThoroughfare = subThoroughfare;
        this.premises = premises;
        this.postalCode = postalCode;
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.phone = phone;
        this.url = url;
    }

    public Locale getLocale() {
        return locale;
    }

    public String getFeatureName() {
        return featureName;
    }

    public String getAdminArea() {
        return adminArea;
    }

    public String getSubAdminArea() {
        return subAdminArea;
    }

    public String getLocality() {
        return locality;
    }

    public String getSubLocality() {
        return subLocality;
    }

    public String getThoroughfare() {
        return thoroughfare;
    }

    public String getSubThoroughfare() {
        return subThoroughfare;
    }

    public String getPremises() {
        return premises;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getPhone() {
        return phone;
    }

    public String getUrl() {
        return url;
    }
}
