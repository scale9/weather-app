package io.ychescale9.weatherapp.data.model;

import android.support.annotation.NonNull;

/**
 * Created by yang on 9/12/16.
 * Forecast weather condition (a snapshot with no aggregated / summary info).
 */
public class ForecastWeatherCondition extends WeatherCondition {

    @NonNull
    public Temperature forecastTemperature;

    public ForecastWeatherCondition(long dateTimeMillis,
                                    @NonNull WeatherType weatherType,
                                    @NonNull Temperature forecastTemperature) {
        super(dateTimeMillis, weatherType);
        this.forecastTemperature = forecastTemperature;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ForecastWeatherCondition that = (ForecastWeatherCondition) o;

        return forecastTemperature.equals(that.forecastTemperature);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + forecastTemperature.hashCode();
        return result;
    }
}
