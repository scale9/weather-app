package io.ychescale9.weatherapp.data.helper;

import android.support.annotation.NonNull;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.ForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.SummaryForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.Temperature;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;
import io.ychescale9.weatherapp.data.model.WeatherType;
import io.ychescale9.weatherapp.util.Clock;

/**
 * Created by yang on 11/12/16.
 */
public class WeatherDataTransformer {

    private final Clock clock;

    public WeatherDataTransformer(Clock clock) {
        this.clock = clock;
    }

    /**
     * Transform current weather condition by setting the dateTime to the actual current time.
     * @param currentWeatherCondition
     * @return
     */
    public CurrentWeatherCondition correctCurrentDateTime(@NonNull CurrentWeatherCondition currentWeatherCondition) {
        currentWeatherCondition.dateTimeMillis = clock.getCurrentTimeMillis();
        return currentWeatherCondition;
    }

    /**
     * Transform forecast weather conditions into daily summaries
     * with maximum and minimum forecast temperatures during the day, and a mostly likely weather type.
     * Assumption: the provided forecastWeatherConditions are in chronological order.
     *
     * Weather type of a day is calculated this way:
     * 1. find the most common weather category (first digit of weather id)
     * 2. get the first item in the group and use it's weather type for the day'as weather type
     * @param forecastWeatherConditions
     * @return
     */
    public List<SummaryForecastWeatherCondition> toDailySummary(@NonNull List<ForecastWeatherCondition> forecastWeatherConditions) {
        // group forecastWeatherConditions by day
        SortedMap<Long, List<ForecastWeatherCondition>> forecastWeatherConditionsByDay = new TreeMap<>();
        for (ForecastWeatherCondition forecastWeatherCondition : forecastWeatherConditions) {
            // use the start date time of the forecastWeatherCondition as the key
            DateTime dateTime = new DateTime(forecastWeatherCondition.dateTimeMillis);
            long startDateTimeMillis = dateTime.withTimeAtStartOfDay().getMillis();
            List<ForecastWeatherCondition> dailyForecastWeatherConditions = forecastWeatherConditionsByDay.get(startDateTimeMillis);
            if (dailyForecastWeatherConditions != null) {
                dailyForecastWeatherConditions.add(forecastWeatherCondition);
            } else {
                dailyForecastWeatherConditions = new ArrayList<>();
                dailyForecastWeatherConditions.add(forecastWeatherCondition);
                forecastWeatherConditionsByDay.put(startDateTimeMillis, dailyForecastWeatherConditions);
            }
        }

        // generate SummaryForecastWeatherCondition for each day
        List<SummaryForecastWeatherCondition> results = new ArrayList<>();

        // if first forecastWeatherCondition is not today (i.e. there's no forecast for the remaining of today)
        // use the first forecastWeatherCondition as the summary forecast for today
        ForecastWeatherCondition firstForecastWeatherCondition = forecastWeatherConditionsByDay.get(forecastWeatherConditionsByDay.firstKey()).get(0);
        DateTime dateTime = new DateTime(firstForecastWeatherCondition.dateTimeMillis);
        long firstForecastStartDateTimeMillis = dateTime.withTimeAtStartOfDay().getMillis();
        long todayStartDateTimeMillis = new DateTime(clock.getCurrentTimeMillis()).withTimeAtStartOfDay().getMillis();
        if (firstForecastStartDateTimeMillis > todayStartDateTimeMillis) {
            // use first forecast tomorrow as the forecast for the remaining of today
            SummaryForecastWeatherCondition summaryForecastWeatherCondition = new SummaryForecastWeatherCondition(
                    new DateTime(clock.getCurrentTimeMillis()).getMillis(),
                    firstForecastWeatherCondition.weatherType,
                    firstForecastWeatherCondition.forecastTemperature,
                    firstForecastWeatherCondition.forecastTemperature
            );
            results.add(summaryForecastWeatherCondition);
        }

        for (Long startDateTimeMillis : forecastWeatherConditionsByDay.keySet()) {
            List<ForecastWeatherCondition> dailyForecastWeatherConditions = forecastWeatherConditionsByDay.get(startDateTimeMillis);
            double maximumTemperature = dailyForecastWeatherConditions.get(0).forecastTemperature.getDegrees();
            double minimumTemperature = dailyForecastWeatherConditions.get(0).forecastTemperature.getDegrees();
            SortedMap<Integer, List<ForecastWeatherCondition>> forecastWeatherConditionsByGroup = new TreeMap<>();
            for (ForecastWeatherCondition weatherCondition : dailyForecastWeatherConditions) {
                if (weatherCondition.forecastTemperature.getDegrees() > maximumTemperature) {
                    maximumTemperature = weatherCondition.forecastTemperature.getDegrees();
                }
                if (weatherCondition.forecastTemperature.getDegrees() < minimumTemperature) {
                    minimumTemperature = weatherCondition.forecastTemperature.getDegrees();
                }

                // use the weather group (1st digit of weather id) as the key
                int weatherGroup = weatherCondition.weatherType.getWeatherId() / 100;
                List<ForecastWeatherCondition> similarForecastWeatherConditions = forecastWeatherConditionsByGroup.get(weatherGroup);
                if (similarForecastWeatherConditions != null) {
                    similarForecastWeatherConditions.add(weatherCondition);
                } else {
                    similarForecastWeatherConditions = new ArrayList<>();
                    similarForecastWeatherConditions.add(weatherCondition);
                    forecastWeatherConditionsByGroup.put(weatherGroup, similarForecastWeatherConditions);
                }
            }

            // find the most common weather group
            List<ForecastWeatherCondition> largestGroupOfSimilarWeatherConditions = forecastWeatherConditionsByGroup.entrySet().iterator().next().getValue();
            for (Integer weatherGroup : forecastWeatherConditionsByGroup.keySet()) {
                List<ForecastWeatherCondition> similarForecastWeatherConditions = forecastWeatherConditionsByGroup.get(weatherGroup);
                if (similarForecastWeatherConditions.size() > largestGroupOfSimilarWeatherConditions.size()) {
                    largestGroupOfSimilarWeatherConditions = similarForecastWeatherConditions;
                }
            }

            // use the first item in the group to represent the weatherType for the day
            WeatherType weatherType = largestGroupOfSimilarWeatherConditions.get(0).weatherType;

            // assemble the SummaryForecastWeatherCondition for the day
            SummaryForecastWeatherCondition summaryForecastWeatherCondition = new SummaryForecastWeatherCondition(
                    startDateTimeMillis,
                    weatherType,
                    new Temperature(TemperatureUnit.CELSIUS, maximumTemperature),
                    new Temperature(TemperatureUnit.CELSIUS, minimumTemperature)
            );
            results.add(summaryForecastWeatherCondition);
        }

        return results;
    }
}
