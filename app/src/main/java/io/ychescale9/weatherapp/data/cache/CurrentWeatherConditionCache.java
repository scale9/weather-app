package io.ychescale9.weatherapp.data.cache;

import android.support.annotation.NonNull;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.ychescale9.weatherapp.data.exception.CacheExpiredException;
import io.ychescale9.weatherapp.data.exception.CacheNotAvailableException;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.util.Clock;
import timber.log.Timber;

/**
 * Created by yang on 9/12/16.
 * Cache layer of the current weather condition
 */
public class CurrentWeatherConditionCache implements Cache<Coordinates, CurrentWeatherCondition> {

    private final int expirationTimeMilliseconds;

    private final Clock clock;

    private Coordinates coordinates;

    private CurrentWeatherCondition cachedCurrentWeather;

    private long lastUpdateTime = 0;

    public CurrentWeatherConditionCache(int expirationTimeMilliseconds, @NonNull Clock clock) {
        this.expirationTimeMilliseconds = expirationTimeMilliseconds;
        this.clock = clock;
    }

    @Override
    public Observable<CurrentWeatherCondition> load(@NonNull final Coordinates coordinates) {
        return Observable.fromCallable(new Callable<CurrentWeatherCondition>() {
            @Override
            public CurrentWeatherCondition call() throws Exception {
                if (cachedCurrentWeather == null) {
                    Timber.d("Current weather condition cache is empty, not returning cache data");
                    throw new CacheNotAvailableException();
                }
                if (CurrentWeatherConditionCache.this.coordinates != null && !CurrentWeatherConditionCache.this.coordinates.equals(coordinates)) {
                    Timber.d("Current weather condition cache is not for %s", coordinates.toString());
                    clear();
                    throw new CacheNotAvailableException();
                }
                if (isExpired()) {
                    Timber.d("Current weather condition cache has expired, not returning cache data");
                    clear();
                    throw new CacheExpiredException();
                }
                return cachedCurrentWeather;
            }
        });
    }

    @Override
    public void update(@NonNull Coordinates coordinates, @NonNull CurrentWeatherCondition currentWeather) {
        // only update cache if new data is NOT empty
        Timber.d("Updating current weather condition cache.");
        // set the coordinates as the key for the cache
        this.coordinates = coordinates;
        // replace existing current weather cache with new current weather
        cachedCurrentWeather = currentWeather;
        // set last update time
        lastUpdateTime = clock.getCurrentTimeMillis();
    }

    @Override
    public void clear() {
        Timber.d("Clearing cache.");
        coordinates = null;
        cachedCurrentWeather = null;
        lastUpdateTime = 0;
    }

    @Override
    public boolean isExpired() {
        return (clock.getCurrentTimeMillis() - lastUpdateTime) > expirationTimeMilliseconds;
    }
}
