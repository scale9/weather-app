package io.ychescale9.weatherapp.data.api;

import java.util.List;

import io.reactivex.Observable;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.ForecastWeatherCondition;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by yang on 9/12/16.
 */
public interface WeatherAppService {

    @GET("weather")
    Observable<CurrentWeatherCondition> loadCurrentWeatherCondition(
            @Query("lat") double latitude,
            @Query("lon") double longitude);

    @GET("forecast")
    Observable<List<ForecastWeatherCondition>> loadForecastWeatherConditions(
            @Query("lat") double latitude,
            @Query("lon") double longitude);
}
