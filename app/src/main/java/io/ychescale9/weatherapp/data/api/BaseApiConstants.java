package io.ychescale9.weatherapp.data.api;

import io.ychescale9.weatherapp.BuildConfig;

/**
 * Created by yang on 14/12/16.
 */
public abstract class BaseApiConstants {

    public abstract String getApiBaseUrl();

    public abstract int getConnectTimeOutMilliseconds();

    public abstract int getReadTimeOutMilliseconds();

    public abstract int getWriteTimeOutMilliseconds();

    public int getAutoRefreshIntervalMilliseconds() {
        return BuildConfig.AUTO_REFRESH_INTERVAL_MILLISECONDS;
    }

    public int getForecastDataUpdateIntervalMilliseconds() {
        return BuildConfig.FORECAST_DATA_UPDATE_INTERVAL_MILLISECONDS;
    }
}
