package io.ychescale9.weatherapp.data.cache;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.weatherapp.data.api.ApiConstants;
import io.ychescale9.weatherapp.util.Clock;

/**
 * Created by yang on 9/12/16.
 */
@Module
public class CacheModule {

    @Provides
    @Singleton
    CurrentWeatherConditionCache provideCurrentWeatherCache(ApiConstants apiConstants, Clock clock) {
        return new CurrentWeatherConditionCache(apiConstants.getAutoRefreshIntervalMilliseconds(), clock);
    }

    @Provides
    @Singleton
    DailyForecastWeatherConditionsCache provideFiveDaysForecastWeathersCache(ApiConstants apiConstants, Clock clock) {
        return new DailyForecastWeatherConditionsCache(apiConstants.getForecastDataUpdateIntervalMilliseconds(), clock);
    }
}
