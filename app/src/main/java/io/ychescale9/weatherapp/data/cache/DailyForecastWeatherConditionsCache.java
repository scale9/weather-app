package io.ychescale9.weatherapp.data.cache;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.ychescale9.weatherapp.data.exception.CacheExpiredException;
import io.ychescale9.weatherapp.data.exception.CacheNotAvailableException;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.SummaryForecastWeatherCondition;
import io.ychescale9.weatherapp.util.Clock;
import timber.log.Timber;

/**
 * Created by yang on 9/12/16.
 * Cache layer of the daily forecast weather conditions
 */
public class DailyForecastWeatherConditionsCache implements Cache<Coordinates, List<SummaryForecastWeatherCondition>> {

    private final int mExpirationTimeMilliseconds;

    private final Clock mClock;

    private Coordinates mCoordinates;

    private final List<SummaryForecastWeatherCondition> mCachedForecastWeathers = new ArrayList<>();

    private long mLastUpdateTime = 0;

    public DailyForecastWeatherConditionsCache(int expirationTimeMilliseconds, @NonNull Clock clock) {
        mExpirationTimeMilliseconds = expirationTimeMilliseconds;
        mClock = clock;
    }

    @Override
    public Observable<List<SummaryForecastWeatherCondition>> load(@NonNull final Coordinates coordinates) {
        return Observable.fromCallable(new Callable<List<SummaryForecastWeatherCondition>>() {
            @Override
            public List<SummaryForecastWeatherCondition> call() throws Exception {
                if (mCachedForecastWeathers.isEmpty()) {
                    Timber.d("Daily forecast weather conditions cache is empty, not returning cache data");
                    throw new CacheNotAvailableException();
                }
                if (mCoordinates != null && !mCoordinates.equals(coordinates)) {
                    Timber.d("Daily forecast weather condition cache is not for %s", coordinates.toString());
                    clear();
                    throw new CacheNotAvailableException();
                }
                if (isExpired()) {
                    Timber.d("Daily forecast weather conditions cache has expired, not returning cache data");
                    clear();
                    throw new CacheExpiredException();
                }
                return new ArrayList<>(mCachedForecastWeathers);
            }
        });
    }

    @Override
    public void update(@NonNull Coordinates coordinates, @NonNull List<SummaryForecastWeatherCondition> forecastWeathers) {
        // only update cache if new data is NOT empty
        if (!forecastWeathers.isEmpty()) {
            Timber.d("Updating forecast weather conditions cache.");
            // set the coordinates as the key for the cache
            mCoordinates = coordinates;
            // replace existing daily forecast weather conditions with new daily forecast weather conditions
            mCachedForecastWeathers.clear();
            mCachedForecastWeathers.addAll(forecastWeathers);
            // set last update time
            mLastUpdateTime = mClock.getCurrentTimeMillis();
        }
    }

    @Override
    public void clear() {
        Timber.d("Clearing cache.");
        mCoordinates = null;
        mCachedForecastWeathers.clear();
        mLastUpdateTime = 0;
    }

    @Override
    public boolean isExpired() {
        return (mClock.getCurrentTimeMillis() - mLastUpdateTime) > mExpirationTimeMilliseconds;
    }
}
