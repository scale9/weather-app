package io.ychescale9.weatherapp.data.model;

import java.util.Locale;

/**
 * Created by yang on 10/12/16.
 */
public class Temperature {

    private final double degrees;
    private final TemperatureUnit unit;

    public Temperature(TemperatureUnit unit, double degrees) {
        this.unit = unit;
        this.degrees = degrees;
    }

    public double getDegrees() {
        return degrees;
    }

    public TemperatureUnit getUnit() {
        return unit;
    }

    public Temperature toFahrenheit() {
        if (unit == TemperatureUnit.FAHRENHEIT) {
            return this;
        }

        double degreesFahrenheit = degrees * 9 / 5 + 32;
        return new Temperature(TemperatureUnit.FAHRENHEIT, degreesFahrenheit);
    }

    public Temperature toCelsius() {
        if (unit == TemperatureUnit.CELSIUS) {
            return this;
        }

        double degreesCelsius = (degrees - 32) *  5 / 9;
        return new Temperature(TemperatureUnit.CELSIUS, degreesCelsius);
    }

    /**
     * Returns the full representation of the temperature,
     * including the rounded temperature value, degree symbol and unit symbol.
     * @return
     */
    public String getFullString() {
        return String.format(Locale.getDefault(), "%d%s%s", (int) Math.round(degrees), "\u00B0", unit.getSymbol());
    }

    /**
     * Returns the short representation of the temperature,
     * including the rounded temperature value and degree symbol.
     * @return
     */
    public String getShortString() {
        return String.format(Locale.getDefault(), "%d%s", (int) Math.round(degrees), "\u00B0");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Temperature that = (Temperature) o;

        if (Double.compare(that.degrees, degrees) != 0) return false;
        return unit == that.unit;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(degrees);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + unit.hashCode();
        return result;
    }
}
