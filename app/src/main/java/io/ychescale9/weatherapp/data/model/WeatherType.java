package io.ychescale9.weatherapp.data.model;

/**
 * Created by yang on 10/12/16.
 */
public class WeatherType {

    private final int weatherId;

    private final String shortDescription;

    private final String longDescription;

    public WeatherType(int weatherId, String shortDescription, String longDescription) {
        this.weatherId = weatherId;
        this.shortDescription = shortDescription;
        this.longDescription = longDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public int getWeatherId() {
        return weatherId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WeatherType that = (WeatherType) o;

        if (weatherId != that.weatherId) return false;
        if (!shortDescription.equals(that.shortDescription)) return false;
        return longDescription.equals(that.longDescription);

    }

    @Override
    public int hashCode() {
        int result = weatherId;
        result = 31 * result + shortDescription.hashCode();
        result = 31 * result + longDescription.hashCode();
        return result;
    }
}
