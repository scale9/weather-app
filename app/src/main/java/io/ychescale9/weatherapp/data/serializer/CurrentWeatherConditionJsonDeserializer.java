package io.ychescale9.weatherapp.data.serializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.Temperature;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;
import io.ychescale9.weatherapp.data.model.WeatherType;

/**
 * Created by yang on 9/12/16.
 */
public class CurrentWeatherConditionJsonDeserializer implements JsonDeserializer<CurrentWeatherCondition> {

    private static final String TAG = CurrentWeatherConditionJsonDeserializer.class.getSimpleName();

    @Override
    public CurrentWeatherCondition deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();

        // parse datetime and convert it to milliseconds
        long dateTimeMillis = jsonObject.get("dt").getAsLong() * 1000;

        // parse weather
        JsonObject weather = jsonObject.getAsJsonArray("weather").get(0).getAsJsonObject();
        int weatherId = weather.get("id").getAsInt();
        String shortDescription = weather.get("main").getAsString();
        String longDescription = weather.get("description").getAsString();
        WeatherType weatherType = new WeatherType(weatherId, shortDescription, longDescription);

        // parse current temperature
        double forecastTemperatureDegrees = jsonObject.get("main").getAsJsonObject().get("temp").getAsDouble();
        // assume returned temperatures are in Celsius
        Temperature currentTemperature = new Temperature(TemperatureUnit.CELSIUS, forecastTemperatureDegrees);

        // parse sunrise and sunset and convert it to milliseconds
        long sunriseDateTimeMillis = jsonObject.get("sys").getAsJsonObject().get("sunrise").getAsLong() * 1000;
        long sunsetDateTimeMillis = jsonObject.get("sys").getAsJsonObject().get("sunset").getAsLong() * 1000;

        return new CurrentWeatherCondition(
                dateTimeMillis,
                weatherType,
                currentTemperature,
                sunriseDateTimeMillis,
                sunsetDateTimeMillis);
    }
}
