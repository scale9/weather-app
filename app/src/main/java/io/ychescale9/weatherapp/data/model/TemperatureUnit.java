package io.ychescale9.weatherapp.data.model;

/**
 * Created by yang on 10/12/16.
 */
public enum TemperatureUnit {
    CELSIUS("Celsius", "C"),
    FAHRENHEIT("Fahrenheit", "F");

    private final String unitName;
    private final String symbol;

    TemperatureUnit(String unitName, String symbol) {
        this.unitName = unitName;
        this.symbol = symbol;
    }

    public String getUnitName() {
        return unitName;
    }

    public String getSymbol() {
        return symbol;
    }

    public static TemperatureUnit fromUnitName(String unitName) {
        if (unitName != null) {
            for (TemperatureUnit unit : TemperatureUnit.values()) {
                if (unitName.equalsIgnoreCase(unit.unitName)) {
                    return unit;
                }
            }
        }
        return null;
    }
}
