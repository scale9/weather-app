package io.ychescale9.weatherapp.data.repository;

import java.util.List;

import io.reactivex.Observable;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.SummaryForecastWeatherCondition;

/**
 * Created by yang on 9/12/16.
 */
public interface WeatherAppRepository {

    Observable<CurrentWeatherCondition> loadCurrentWeatherCondition(Coordinates coordinates);

    Observable<List<SummaryForecastWeatherCondition>> loadDailyForecastWeatherConditions(Coordinates coordinates);
}
