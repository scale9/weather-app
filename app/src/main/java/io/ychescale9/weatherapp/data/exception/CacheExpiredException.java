package io.ychescale9.weatherapp.data.exception;

/**
 * Created by yang on 9/12/16.
 * Exception for when the cache has expired.
 */
public class CacheExpiredException extends Exception {

    public CacheExpiredException() {
        super();
    }

    public CacheExpiredException(final String message) {
        super(message);
    }

    public CacheExpiredException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CacheExpiredException(final Throwable cause) {
        super(cause);
    }
}
