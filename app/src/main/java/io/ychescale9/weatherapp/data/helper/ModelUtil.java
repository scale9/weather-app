package io.ychescale9.weatherapp.data.helper;

import android.location.Address;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import io.ychescale9.weatherapp.data.model.SimpleAddress;

/**
 * Created by yang on 29/4/17.
 */
public final class ModelUtil {

    public static SimpleAddress toSimpleAddress(@NonNull Address address) {
        return new SimpleAddress(
                address.getLocale(),
                address.getFeatureName(),
                address.getAdminArea(),
                address.getSubAdminArea(),
                address.getLocality(),
                address.getSubLocality(),
                address.getThoroughfare(),
                address.getSubThoroughfare(),
                address.getPremises(),
                address.getPostalCode(),
                address.getCountryCode(),
                address.getCountryName(),
                address.getLatitude(),
                address.getLongitude(),
                address.getPhone(),
                address.getUrl()
        );
    }

    public static List<SimpleAddress> toSimpleAddressList(@NonNull List<Address> addressList) {
        List<SimpleAddress> simpleAddresses = new ArrayList<>(addressList.size());
        for (Address address : addressList) {
            SimpleAddress simpleAddress = new SimpleAddress(
                    address.getLocale(),
                    address.getFeatureName(),
                    address.getAdminArea(),
                    address.getSubAdminArea(),
                    address.getLocality(),
                    address.getSubLocality(),
                    address.getThoroughfare(),
                    address.getSubThoroughfare(),
                    address.getPremises(),
                    address.getPostalCode(),
                    address.getCountryCode(),
                    address.getCountryName(),
                    address.getLatitude(),
                    address.getLongitude(),
                    address.getPhone(),
                    address.getUrl()
            );
            simpleAddresses.add(simpleAddress);
        }

        return simpleAddresses;
    }
}
