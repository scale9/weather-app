package io.ychescale9.weatherapp.data.model;

import android.support.annotation.NonNull;

/**
 * Created by yang on 10/12/16.
 * Base class for Classes representing a weather condition
 */
public abstract class WeatherCondition {

    public long dateTimeMillis;

    @NonNull
    public WeatherType weatherType;

    public WeatherCondition(long dateTimeMillis, @NonNull WeatherType weatherType) {
        this.dateTimeMillis = dateTimeMillis;
        this.weatherType = weatherType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WeatherCondition that = (WeatherCondition) o;

        if (dateTimeMillis != that.dateTimeMillis) return false;
        return weatherType.equals(that.weatherType);

    }

    @Override
    public int hashCode() {
        int result = (int) (dateTimeMillis ^ (dateTimeMillis >>> 32));
        result = 31 * result + weatherType.hashCode();
        return result;
    }
}
