package io.ychescale9.weatherapp.data.disk;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import io.ychescale9.weatherapp.R;

/**
 * Created by yang on 12/12/16.
 */
public class SharedPreferencesManagerImpl implements SharedPreferencesManager {

    private final String sharedPreferencesFileName;
    private final Context context;
    private final SharedPreferences sharedPreferences;

    public SharedPreferencesManagerImpl(@NonNull String sharedPreferencesFileName, @NonNull Context context) {
        this.sharedPreferencesFileName = sharedPreferencesFileName;
        this.context = context;
        sharedPreferences = context.getSharedPreferences(this.sharedPreferencesFileName, Context.MODE_PRIVATE);
    }

    @Override
    public void clearAll() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().apply();
    }

    @Override
    public void setTemperatureUnit(String temperatureUnit) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getString(R.string.pref_key_temperature_unit), temperatureUnit);
        editor.apply();
    }

    @Override
    public String getTemperatureUnit(String defaultTemperatureUnit) {
        return sharedPreferences.getString(context.getString(R.string.pref_key_temperature_unit), defaultTemperatureUnit);
    }
}
