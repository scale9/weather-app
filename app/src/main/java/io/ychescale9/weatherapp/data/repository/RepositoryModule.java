package io.ychescale9.weatherapp.data.repository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.weatherapp.data.api.WeatherAppService;
import io.ychescale9.weatherapp.data.cache.CurrentWeatherConditionCache;
import io.ychescale9.weatherapp.data.cache.DailyForecastWeatherConditionsCache;
import io.ychescale9.weatherapp.data.helper.WeatherDataTransformer;

/**
 * Created by yang on 9/12/16.
 */
@Module
public class RepositoryModule {

    @Provides
    @Singleton
    WeatherAppRepository provideWeatherAppRepository(CurrentWeatherConditionCache currentWeatherConditionCache,
                                                     DailyForecastWeatherConditionsCache dailyForecastWeatherConditionsCache,
                                                     WeatherAppService weatherAppService,
                                                     WeatherDataTransformer weatherDataTransformer) {
        return new WeatherAppRepositoryImpl(
                currentWeatherConditionCache,
                dailyForecastWeatherConditionsCache,
                weatherAppService,
                weatherDataTransformer
        );
    }
}
