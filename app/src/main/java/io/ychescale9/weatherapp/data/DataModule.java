package io.ychescale9.weatherapp.data;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.weatherapp.R;
import io.ychescale9.weatherapp.data.disk.SharedPreferencesManager;
import io.ychescale9.weatherapp.data.disk.SharedPreferencesManagerImpl;
import io.ychescale9.weatherapp.data.helper.WeatherDataTransformer;
import io.ychescale9.weatherapp.data.serializer.CurrentWeatherConditionJsonDeserializer;
import io.ychescale9.weatherapp.data.serializer.ForecastWeatherConditionJsonDeserializer;
import io.ychescale9.weatherapp.util.Clock;

/**
 * Created by yang on 10/12/16.
 */
@Module
public class DataModule {

    @Provides
    @Singleton
    CurrentWeatherConditionJsonDeserializer provideCurrentWeatherConditionJsonDeserializer() {
        return new CurrentWeatherConditionJsonDeserializer();
    }

    @Provides
    @Singleton
    ForecastWeatherConditionJsonDeserializer provideForecastWeatherConditionJsonDeserializer() {
        return new ForecastWeatherConditionJsonDeserializer();
    }

    @Provides
    @Singleton
    WeatherDataTransformer provideForecastWeatherConditionsTransformer(Clock clock) {
        return new WeatherDataTransformer(clock);
    }

    @Provides
    @Singleton
    SharedPreferencesManager provideSharedPreferencesManager(Application application) {
        String sharedPreferencesFileName = application.getApplicationContext().getString(R.string.shared_prefs_file_name);
        return new SharedPreferencesManagerImpl(sharedPreferencesFileName, application.getApplicationContext());
    }
}
