package io.ychescale9.weatherapp.data.model;

import android.support.annotation.NonNull;

/**
 * Created by yang on 9/12/16.
 * Summary forecast weather condition with max and min temperatures of the period.
 */
public class SummaryForecastWeatherCondition extends WeatherCondition {

    @NonNull
    public Temperature forecastMaxTemperature;

    @NonNull
    public Temperature forecastMinTemperature;

    public SummaryForecastWeatherCondition(long dateTimeMillis,
                                    @NonNull WeatherType weatherType,
                                    @NonNull Temperature forecastMaxTemperature,
                                    @NonNull Temperature forecastMinTemperature) {
        super(dateTimeMillis, weatherType);
        this.forecastMaxTemperature = forecastMaxTemperature;
        this.forecastMinTemperature = forecastMinTemperature;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SummaryForecastWeatherCondition that = (SummaryForecastWeatherCondition) o;

        if (!forecastMaxTemperature.equals(that.forecastMaxTemperature)) return false;
        return forecastMinTemperature.equals(that.forecastMinTemperature);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + forecastMaxTemperature.hashCode();
        result = 31 * result + forecastMinTemperature.hashCode();
        return result;
    }
}
