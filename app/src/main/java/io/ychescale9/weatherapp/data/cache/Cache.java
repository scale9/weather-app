package io.ychescale9.weatherapp.data.cache;

import io.reactivex.Observable;

/**
 * Created by yang on 9/12/16.
 */
interface Cache<K, T> {

    Observable<T> load(K k);

    void update(K k, T t);

    void clear();

    boolean isExpired();
}
