package io.ychescale9.weatherapp.data.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by yang on 9/12/16.
 * The current weather condition.
 */
public class CurrentWeatherCondition extends WeatherCondition {

    @NonNull
    public Temperature currentTemperature;

    @Nullable
    public Temperature forecastMaxTemperature;

    @Nullable
    public Temperature forecastMinTemperature;

    public long sunriseDateTimeMillis;

    public long sunsetDateTimeMillis;

    public CurrentWeatherCondition(long dateTimeMillis,
                                   @NonNull WeatherType weatherType,
                                   @NonNull Temperature currentTemperature,
                                   long sunriseDateTimeMillis,
                                   long sunsetDateTimeMillis) {
        super(dateTimeMillis, weatherType);
        this.currentTemperature = currentTemperature;
        this.sunriseDateTimeMillis = sunriseDateTimeMillis;
        this.sunsetDateTimeMillis = sunsetDateTimeMillis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CurrentWeatherCondition that = (CurrentWeatherCondition) o;

        if (sunriseDateTimeMillis != that.sunriseDateTimeMillis) return false;
        if (sunsetDateTimeMillis != that.sunsetDateTimeMillis) return false;
        if (!currentTemperature.equals(that.currentTemperature)) return false;
        if (forecastMaxTemperature != null ? !forecastMaxTemperature.equals(that.forecastMaxTemperature) : that.forecastMaxTemperature != null)
            return false;
        return forecastMinTemperature != null ? forecastMinTemperature.equals(that.forecastMinTemperature) : that.forecastMinTemperature == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + currentTemperature.hashCode();
        result = 31 * result + (forecastMaxTemperature != null ? forecastMaxTemperature.hashCode() : 0);
        result = 31 * result + (forecastMinTemperature != null ? forecastMinTemperature.hashCode() : 0);
        result = 31 * result + (int) (sunriseDateTimeMillis ^ (sunriseDateTimeMillis >>> 32));
        result = 31 * result + (int) (sunsetDateTimeMillis ^ (sunsetDateTimeMillis >>> 32));
        return result;
    }
}
