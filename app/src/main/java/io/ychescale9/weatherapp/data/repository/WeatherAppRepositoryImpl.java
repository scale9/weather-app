package io.ychescale9.weatherapp.data.repository;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.ychescale9.weatherapp.data.api.WeatherAppService;
import io.ychescale9.weatherapp.data.cache.CurrentWeatherConditionCache;
import io.ychescale9.weatherapp.data.cache.DailyForecastWeatherConditionsCache;
import io.ychescale9.weatherapp.data.helper.WeatherDataTransformer;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.ForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.SummaryForecastWeatherCondition;
import timber.log.Timber;

/**
 * Created by yang on 9/12/16.
 */
public class WeatherAppRepositoryImpl implements WeatherAppRepository {

    private final CurrentWeatherConditionCache mCurrentWeatherCache;
    private final DailyForecastWeatherConditionsCache mDailyForecastWeathersCache;
    private final WeatherAppService mWeatherAppService;
    private final WeatherDataTransformer mWeatherDataTransformer;

    WeatherAppRepositoryImpl(@NonNull CurrentWeatherConditionCache currentWeatherConditionCache,
                                    @NonNull DailyForecastWeatherConditionsCache dailyForecastWeatherConditionsCache,
                                    @NonNull WeatherAppService weatherAppService,
                                    @NonNull WeatherDataTransformer weatherDataTransformer) {
        mCurrentWeatherCache = currentWeatherConditionCache;
        mDailyForecastWeathersCache = dailyForecastWeatherConditionsCache;
        mWeatherAppService = weatherAppService;
        mWeatherDataTransformer = weatherDataTransformer;
    }

    @Override
    public Observable<CurrentWeatherCondition> loadCurrentWeatherCondition(@NonNull final Coordinates coordinates) {
        // try to load current weather condition from cache first,
        // if cache does not exist or has expired load data from service
        Observable<CurrentWeatherCondition> loadDataFromService = mWeatherAppService
                .loadCurrentWeatherCondition(
                        coordinates.getLatitude(), coordinates.getLongitude()
                )
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        Timber.d("Loading current weather from server.");
                    }
                })
                .map(new Function<CurrentWeatherCondition, CurrentWeatherCondition>() {
                    @Override
                    public CurrentWeatherCondition apply(CurrentWeatherCondition currentWeatherCondition) throws Exception {
                        // overwrite the dateTime of the currentWeatherCondition with actual current time
                        return mWeatherDataTransformer.correctCurrentDateTime(currentWeatherCondition);
                    }
                })
                .doOnNext(new Consumer<CurrentWeatherCondition>() {
                    @Override
                    public void accept(CurrentWeatherCondition currentWeatherCondition) throws Exception {
                        Timber.d("Data loaded from server.");

                        // update cache once current weather condition has been loaded from server
                        Timber.d("Updating cache with loaded current weather condition from server");
                        mCurrentWeatherCache.update(coordinates, currentWeatherCondition);
                    }
                });

        return mCurrentWeatherCache.load(coordinates)
                .onExceptionResumeNext(loadDataFromService);
    }

    @Override
    public Observable<List<SummaryForecastWeatherCondition>> loadDailyForecastWeatherConditions(@NonNull final Coordinates coordinates) {
        // try to load daily forecast weather conditions from cache first,
        // if cache does not exist or has expired load data from service,
        // then convert snapshots of forecast weather conditions to daily forecast weather conditions.
        Observable<List<SummaryForecastWeatherCondition>> loadDataFromService = mWeatherAppService
                .loadForecastWeatherConditions(
                        coordinates.getLatitude(), coordinates.getLongitude()
                )
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        Timber.d("Loading forecast weather conditions from server.");
                    }
                })
                .map(new Function<List<ForecastWeatherCondition>, List<SummaryForecastWeatherCondition>>() {
                    @Override
                    public List<SummaryForecastWeatherCondition> apply(List<ForecastWeatherCondition> forecastWeatherConditions) throws Exception {
                        // transform the snapshots of forecast weather conditions to daily summaries
                        return mWeatherDataTransformer.toDailySummary(forecastWeatherConditions);
                    }
                })
                .doOnNext(new Consumer<List<SummaryForecastWeatherCondition>>() {
                    @Override
                    public void accept(List<SummaryForecastWeatherCondition> dailyForecastWeatherConditions) throws Exception {
                        Timber.d("Data loaded from server.");

                        // update cache once daily forecast weather conditions have been loaded from server
                        if (!dailyForecastWeatherConditions.isEmpty()) {
                            Timber.d("Updating cache with loaded daily forecast weather conditions from server");
                            mDailyForecastWeathersCache.update(coordinates, dailyForecastWeatherConditions);
                        }
                    }
                });

        return mDailyForecastWeathersCache.load(coordinates)
                .onExceptionResumeNext(loadDataFromService)
                .takeWhile(new Predicate<List<SummaryForecastWeatherCondition>>() {
                    @Override
                    public boolean test(List<SummaryForecastWeatherCondition> forecastWeathers) throws Exception {
                        return !forecastWeathers.isEmpty();
                    }
                });
    }
}
