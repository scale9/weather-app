package io.ychescale9.weatherapp.data.disk;

/**
 * Created by yang on 12/12/16.
 */
public interface SharedPreferencesManager {

    void clearAll();

    void setTemperatureUnit(String temperatureUnit);

    String getTemperatureUnit(String defaultTemperatureUnit);
}
