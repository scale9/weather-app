package io.ychescale9.weatherapp.data.exception;

/**
 * Created by yang on 9/12/16.
 * Exception for when the cache is empty.
 */
public class CacheNotAvailableException extends Exception {

    public CacheNotAvailableException() {
        super();
    }

    public CacheNotAvailableException(final String message) {
        super(message);
    }

    public CacheNotAvailableException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CacheNotAvailableException(final Throwable cause) {
        super(cause);
    }
}
