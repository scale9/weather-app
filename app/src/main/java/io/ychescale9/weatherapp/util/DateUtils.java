package io.ychescale9.weatherapp.util;

import org.joda.time.DateTime;

/**
 * Created by yang on 11/12/16.
 */
public final class DateUtils {

    /**
     * Return true if instant1 and instant 2 are on the same day
     * @param instant1 - UTC time in milliseconds
     * @param instant2 - UTC time in milliseconds
     * @return
     */
    public static boolean isSameDay(long instant1, long instant2) {
        return new DateTime(instant1).withTimeAtStartOfDay().isEqual(new DateTime(instant2).withTimeAtStartOfDay());
    }
}
