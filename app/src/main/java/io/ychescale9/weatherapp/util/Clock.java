package io.ychescale9.weatherapp.util;

/**
 * Created by yang on 9/12/16.
 */
public interface Clock {

    /**
     * Returns the current time in milliseconds UTC.
     * @return
     */
    long getCurrentTimeMillis();
}
