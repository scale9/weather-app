package io.ychescale9.weatherapp.util;

import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.util.List;

import io.ychescale9.weatherapp.data.model.SimpleAddress;

import static io.ychescale9.weatherapp.data.helper.ModelUtil.toSimpleAddressList;

/**
 * Created by yang on 13/12/16.
 */
public class GeocodingHelperImpl implements GeocodingHelper {

    private final Geocoder geocoder;

    public GeocodingHelperImpl(@NonNull Geocoder geocoder) {
        this.geocoder = geocoder;
    }

    public List<SimpleAddress> getFromLocation(double latitude, double longitude, int maxResults) throws IOException {
        List<Address> addressList = geocoder.getFromLocation(latitude, longitude, maxResults);
        // convert android.location.Address to SimpleAddress
        return toSimpleAddressList(addressList);
    }
}
