package io.ychescale9.weatherapp.util;

import android.location.Location;
import android.support.annotation.NonNull;

import com.google.android.gms.location.LocationListener;

import java.lang.ref.WeakReference;

/**
 * Created by yang on 16/12/16.
 * Helper class to avoid a memory leak from Google Play Services
 */
public class WeakLocationListener implements LocationListener {

    private final WeakReference<LocationListener> locationListenerRef;

    public WeakLocationListener(@NonNull LocationListener locationListener) {
        locationListenerRef = new WeakReference<>(locationListener);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (locationListenerRef.get() != null) {
            locationListenerRef.get().onLocationChanged(location);
        }
    }

}

