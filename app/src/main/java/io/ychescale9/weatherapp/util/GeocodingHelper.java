package io.ychescale9.weatherapp.util;

import java.io.IOException;
import java.util.List;

import io.ychescale9.weatherapp.data.model.SimpleAddress;

/**
 * Created by yang on 13/12/16.
 */
public interface GeocodingHelper {

    List<SimpleAddress> getFromLocation(double latitude, double longitude, int maxResults) throws IOException;
}
