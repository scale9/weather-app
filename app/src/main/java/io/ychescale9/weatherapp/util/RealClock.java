package io.ychescale9.weatherapp.util;

/**
 * Created by yang on 9/12/16.
 */
public class RealClock implements Clock {

    @Override
    public long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }
}
