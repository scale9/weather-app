package io.ychescale9.weatherapp;

import io.ychescale9.weatherapp.data.model.Coordinates;

/**
 * Created by yang on 15/12/16.
 */
public class AppEnvironment extends BaseAppEnvironment {

    @Override
    public Coordinates getFixedLocation() {
        // Don't provide any fixed location for release build
        return null;
    }
}
