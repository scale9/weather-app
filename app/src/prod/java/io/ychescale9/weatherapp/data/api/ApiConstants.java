package io.ychescale9.weatherapp.data.api;

import io.ychescale9.weatherapp.BuildConfig;

/**
 * Created by yang on 9/12/16.
 * Provides methods for getting api constant values
 */
public class ApiConstants extends BaseApiConstants {

    public String getApiBaseUrl() {
        return BuildConfig.API_BASE_URL;
    }

    public int getConnectTimeOutMilliseconds() {
        return BuildConfig.API_CONNECT_TIMEOUT_MILLISECONDS;
    }

    public int getReadTimeOutMilliseconds() {
        return BuildConfig.API_READ_TIMEOUT_MILLISECONDS;
    }

    public int getWriteTimeOutMilliseconds() {
        return BuildConfig.API_WRITE_TIMEOUT_MILLISECONDS;
    }

    public int getAutoRefreshIntervalMilliseconds() {
        return BuildConfig.AUTO_REFRESH_INTERVAL_MILLISECONDS;
    }

    public int getForecastDataUpdateIntervalMilliseconds() {
        return BuildConfig.FORECAST_DATA_UPDATE_INTERVAL_MILLISECONDS;
    }
}
