package io.ychescale9.weatherapp.data.api;

import android.app.Application;
import android.location.Geocoder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.weatherapp.BuildConfig;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.ForecastWeatherCondition;
import io.ychescale9.weatherapp.data.serializer.CurrentWeatherConditionJsonDeserializer;
import io.ychescale9.weatherapp.data.serializer.ForecastWeatherConditionJsonDeserializer;
import io.ychescale9.weatherapp.data.serializer.ForecastWeatherTypeAdapterFactory;
import io.ychescale9.weatherapp.util.GeocodingHelper;
import io.ychescale9.weatherapp.util.GeocodingHelperImpl;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by yang on 9/12/16.
 */
@Module
public class ApiModule {

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(ApiConstants apiConstants) {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(apiConstants.getConnectTimeOutMilliseconds(), TimeUnit.MILLISECONDS)
                .readTimeout(apiConstants.getReadTimeOutMilliseconds(), TimeUnit.MILLISECONDS)
                .writeTimeout(apiConstants.getWriteTimeOutMilliseconds(), TimeUnit.MILLISECONDS);
        // append api key to every requests
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        // use celsius as temperature unit
                        .addQueryParameter("units", "metric")
                        // add api key
                        .addQueryParameter("apikey", BuildConfig.API_KEY)
                        .build();

                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        // add logging interceptor
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        builder.addInterceptor(loggingInterceptor);
        return builder.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client,
                             ApiConstants apiConstants,
                             CurrentWeatherConditionJsonDeserializer currentWeatherJsonDeserializer,
                             ForecastWeatherConditionJsonDeserializer forecastWeatherJsonDeserializer) {
        // create Gson object with custom deserializer
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(CurrentWeatherCondition.class, currentWeatherJsonDeserializer);
        gsonBuilder.registerTypeAdapter(ForecastWeatherCondition.class, forecastWeatherJsonDeserializer);
        gsonBuilder.registerTypeAdapterFactory(new ForecastWeatherTypeAdapterFactory());
        Gson gson = gsonBuilder.create();

        return new Retrofit.Builder()
                .baseUrl(apiConstants.getApiBaseUrl())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }

    @Provides
    @Singleton
    WeatherAppService provideWeatherAppService(Retrofit retrofit) {
        return retrofit.create(WeatherAppService.class);
    }

    @Provides
    @Singleton
    GeocodingHelper provideGeocodingHelper(Application application) {
        Geocoder geocoder = new Geocoder(application.getApplicationContext(), Locale.getDefault());
        return new GeocodingHelperImpl(geocoder);
    }
}
