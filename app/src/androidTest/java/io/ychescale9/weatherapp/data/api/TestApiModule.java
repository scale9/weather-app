package io.ychescale9.weatherapp.data.api;

import android.location.Address;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.ForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.SimpleAddress;
import io.ychescale9.weatherapp.data.serializer.CurrentWeatherConditionJsonDeserializer;
import io.ychescale9.weatherapp.data.serializer.ForecastWeatherConditionJsonDeserializer;
import io.ychescale9.weatherapp.data.serializer.ForecastWeatherTypeAdapterFactory;
import io.ychescale9.weatherapp.util.GeocodingHelper;
import io.ychescale9.weatherapp.util.TestData;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static io.ychescale9.weatherapp.data.helper.ModelUtil.toSimpleAddress;

/**
 * Created by yang on 22/10/16.
 */
@Module
public class TestApiModule {

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(ApiConstants apiConstants) {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(apiConstants.getConnectTimeOutMilliseconds(), TimeUnit.MILLISECONDS)
                .readTimeout(apiConstants.getReadTimeOutMilliseconds(), TimeUnit.MILLISECONDS)
                .writeTimeout(apiConstants.getWriteTimeOutMilliseconds(), TimeUnit.MILLISECONDS);
        return builder.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client,
                             ApiConstants apiConstants,
                             CurrentWeatherConditionJsonDeserializer currentWeatherJsonDeserializer,
                             ForecastWeatherConditionJsonDeserializer forecastWeatherJsonDeserializer) {
        // create Gson object with custom deserializer
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(CurrentWeatherCondition.class, currentWeatherJsonDeserializer);
        gsonBuilder.registerTypeAdapter(ForecastWeatherCondition.class, forecastWeatherJsonDeserializer);
        gsonBuilder.registerTypeAdapterFactory(new ForecastWeatherTypeAdapterFactory());
        Gson gson = gsonBuilder.create();

        return new Retrofit.Builder()
                .baseUrl(apiConstants.getApiBaseUrl())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }

    @Provides
    @Singleton
    WeatherAppService provideWeatherAppService(Retrofit retrofit) {
        return retrofit.create(WeatherAppService.class);
    }

    @Provides
    @Singleton
    GeocodingHelper provideGeocodingHelper() {
        // create a mock address
        final Address mockAddress = new Address(Locale.getDefault());
        // we only care about the locality (city name)
        mockAddress.setLocality(TestData.CITY_NAME);
        mockAddress.setLongitude(TestData.COORDINATES.getLongitude());
        mockAddress.setLatitude(TestData.COORDINATES.getLatitude());
        return new GeocodingHelper() {
            @Override
            public List<SimpleAddress> getFromLocation(double latitude, double longitude, int maxResults) throws IOException {
                return Collections.singletonList(toSimpleAddress(mockAddress));
            }
        };
    }
}
