package io.ychescale9.weatherapp.data.api;

/**
 * Created by yang on 14/12/16.
 */
public class TestApiConstants extends ApiConstants {

    private final int mockServerPort;

    public TestApiConstants(int mockServerPort) {
        this.mockServerPort = mockServerPort;
    }

    @Override
    public String getApiBaseUrl() {
        return "http://127.0.0.1:" + mockServerPort + "/";
    }

    @Override
    public int getConnectTimeOutMilliseconds() {
        return 0;
    }

    @Override
    public int getReadTimeOutMilliseconds() {
        return 0;
    }

    @Override
    public int getWriteTimeOutMilliseconds() {
        return 0;
    }
}
