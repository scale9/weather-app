package io.ychescale9.weatherapp.data.disk;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.ychescale9.weatherapp.R;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by yang on 12/12/16.
 * Unit tests for the implementation of {@link SharedPreferencesManagerImpl}
 */
@RunWith(AndroidJUnit4.class)
@MediumTest
public class SharedPreferencesManagerImplTest {

    private final String TEST_SHARED_PREFERENCES_FILE_NAME = "test_weather_app_preferences";
    private final String DEFAULT_TEMPERATURE_UNIT = TemperatureUnit.CELSIUS.getUnitName();

    private SharedPreferences sharedPreferences;
    private SharedPreferencesManagerImpl sharedPreferencesManager;

    @Before
    public void setUp() {
        sharedPreferences = getTargetContext().getSharedPreferences(TEST_SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        sharedPreferencesManager = new SharedPreferencesManagerImpl(
                TEST_SHARED_PREFERENCES_FILE_NAME,
                getTargetContext()
        );
    }

    @After
    public void tearDown() {
        // clear all shared preferences entries
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().apply();
    }

    @Test
    public void test_clearAll() {
        // set preferences
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getTargetContext().getString(R.string.pref_key_temperature_unit), TemperatureUnit.FAHRENHEIT.getUnitName());
        editor.apply();

        // clear all preferences
        sharedPreferencesManager.clearAll();

        // should NOT have preferences
        assertThat(sharedPreferences.getAll().isEmpty(), is(true));
    }

    @Test
    public void test_setTemperatureUnit() {
        // set temperatureUnit preference
        sharedPreferencesManager.setTemperatureUnit(TemperatureUnit.FAHRENHEIT.getUnitName());
        // should return correct temperatureUnit
        assertThat(sharedPreferences.getString(getTargetContext().getString(R.string.pref_key_temperature_unit), DEFAULT_TEMPERATURE_UNIT), is(TemperatureUnit.FAHRENHEIT.getUnitName()));

        // set temperatureUnit preference
        sharedPreferencesManager.setTemperatureUnit(TemperatureUnit.CELSIUS.getUnitName());
        // should return correct temperatureUnit
        assertThat(sharedPreferences.getString(getTargetContext().getString(R.string.pref_key_temperature_unit), DEFAULT_TEMPERATURE_UNIT), is(TemperatureUnit.CELSIUS.getUnitName()));
    }

    @Test
    public void test_getTemperatureUnit() {
        // set temperatureUnit preference using native SharedPreferences
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getTargetContext().getString(R.string.pref_key_temperature_unit), TemperatureUnit.FAHRENHEIT.getUnitName());
        editor.apply();
        // should return correct temperateUnit
        assertThat(sharedPreferencesManager.getTemperatureUnit(DEFAULT_TEMPERATURE_UNIT), is(TemperatureUnit.FAHRENHEIT.getUnitName()));

        // set temperatureUnit preference using native SharedPreferences
        editor.putString(getTargetContext().getString(R.string.pref_key_temperature_unit), TemperatureUnit.CELSIUS.getUnitName());
        editor.apply();
        // should return correct temperateUnit
        assertThat(sharedPreferencesManager.getTemperatureUnit(DEFAULT_TEMPERATURE_UNIT), is(TemperatureUnit.CELSIUS.getUnitName()));
    }

    @Test
    public void test_getTemperatureUnit_default() {
        // should return the default value
        assertThat(sharedPreferencesManager.getTemperatureUnit(DEFAULT_TEMPERATURE_UNIT), is(DEFAULT_TEMPERATURE_UNIT));
    }
}
