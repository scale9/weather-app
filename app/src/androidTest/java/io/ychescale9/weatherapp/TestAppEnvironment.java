package io.ychescale9.weatherapp;

import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.util.TestData;

/**
 * Created by yang on 15/12/16.
 */
public class TestAppEnvironment extends AppEnvironment {

    @Override
    public Coordinates getFixedLocation() {
        // provide a mock location as the fixed location for android test
        return TestData.COORDINATES;
    }
}
