package io.ychescale9.weatherapp.ui;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.ychescale9.weatherapp.presentation.home.HomeActivity;

import static junit.framework.Assert.assertTrue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assume.assumeThat;

/**
 * Created by yang on 15/12/16.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class HomeScreenTest extends BaseUiTest {

    @Rule
    public IntentsTestRule<HomeActivity> activityRule = new IntentsTestRule<>(
            HomeActivity.class,
            true,
            false); // launch activity manually

    @Override
    public void setUp() throws Throwable {
        super.setUp();

        // only execute tests if Google API (Google Play Services) is available on the device
        assumeThat(GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(testApp.getApplicationContext()), is(ConnectionResult.SUCCESS));
    }

    @Override
    public void tearDown() throws Throwable {
        super.tearDown();
    }

    @Test
    public void openHomeScreen() throws Throwable {
        assertTrue(true);
    }

}

