package io.ychescale9.weatherapp.ui;

import android.app.Instrumentation;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;

import javax.inject.Inject;

import io.ychescale9.weatherapp.TestComponent;
import io.ychescale9.weatherapp.TestWeatherAppApplication;
import io.ychescale9.weatherapp.TimeZoneUtcRule;
import io.ychescale9.weatherapp.data.cache.CurrentWeatherConditionCache;
import io.ychescale9.weatherapp.data.cache.DailyForecastWeatherConditionsCache;
import io.ychescale9.weatherapp.data.serializer.CurrentWeatherConditionJsonDeserializer;
import io.ychescale9.weatherapp.data.serializer.ForecastWeatherConditionJsonDeserializer;
import io.ychescale9.weatherapp.util.Clock;
import io.ychescale9.weatherapp.util.SystemAnimationsUtil;
import io.ychescale9.weatherapp.util.TestClock;
import io.ychescale9.weatherapp.util.TestUtil;
import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.InstrumentationRegistry.getInstrumentation;

/**
 * Created by yang on 15/12/16.
 */
public abstract class BaseUiTest {

    @Rule
    public TimeZoneUtcRule timeZoneUtcRule = new TimeZoneUtcRule();

    @Inject
    CurrentWeatherConditionJsonDeserializer currentWeatherConditionJsonDeserializer;

    @Inject
    ForecastWeatherConditionJsonDeserializer forecastWeatherConditionJsonDeserializer;

    @Inject
    CurrentWeatherConditionCache currentWeatherConditionCache;

    @Inject
    DailyForecastWeatherConditionsCache dailyForecastWeatherConditionsCache;

    @Inject
    Clock testClock;

    MockWebServer mockWebServer;

    TestWeatherAppApplication testApp;

    @BeforeClass
    public static void setUpBeforeClass() throws Throwable {
        // disable animations before running tests
        SystemAnimationsUtil.disableAnimations(getInstrumentation().getTargetContext());
    }

    @AfterClass
    public static void tearDownAfterClass() throws Throwable {
        // re-enabled animations after running tests
        SystemAnimationsUtil.enableAnimations(getInstrumentation().getTargetContext());
    }

    @Before
    public void setUp() throws Throwable {
        Instrumentation instrumentation = getInstrumentation();
        testApp = (TestWeatherAppApplication) instrumentation.getTargetContext().getApplicationContext();
        TestComponent component = (TestComponent) testApp.getComponent();
        component.inject(this);

        // start a new mock server
        mockWebServer = new MockWebServer();
        mockWebServer.start(TestUtil.MOCK_SERVER_PORT);
    }

    @After
    public void tearDown() throws Throwable {
        // clear cache data after each test
        currentWeatherConditionCache.clear();
        dailyForecastWeatherConditionsCache.clear();

        // reset the test clock
        getTestClock().reset();

        // shut down mock server
        mockWebServer.shutdown();
    }

    TestClock getTestClock() {
        if (testClock instanceof TestClock) {
            return (TestClock) testClock;
        } else {
            return null;
        }
    }
}
