package io.ychescale9.weatherapp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.weatherapp.util.Clock;
import io.ychescale9.weatherapp.util.TestClock;

/**
 * Created by yang on 16/12/16.
 */
@Module
public class TestTimeModule {

    @Provides
    @Singleton
    Clock provideClock() {
        return new TestClock();
    }
}
