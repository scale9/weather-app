package io.ychescale9.weatherapp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.weatherapp.util.SchedulerProvider;
import io.ychescale9.weatherapp.util.UiTestSchedulerProvider;

/**
 * Created by yang on 29/4/17.
 */
@Module
public class TestAppModule {

    @Provides
    @Singleton
    SchedulerProvider provideSchedulerProvider() {
        return new UiTestSchedulerProvider();
    }
}
