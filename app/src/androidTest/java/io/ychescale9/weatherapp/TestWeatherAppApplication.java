package io.ychescale9.weatherapp;

/**
 * Created by yang on 14/12/16.
 */
public class TestWeatherAppApplication extends WeatherAppApplication {

    /**
     * Create top-level Dagger test component.
     */
    @Override
    protected AppComponent createComponent() {
        return DaggerTestComponent.builder()
                .app(this)
                .build();
    }
}
