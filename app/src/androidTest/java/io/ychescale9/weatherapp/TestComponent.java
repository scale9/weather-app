package io.ychescale9.weatherapp;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import io.ychescale9.weatherapp.data.DataModule;
import io.ychescale9.weatherapp.data.api.TestApiModule;
import io.ychescale9.weatherapp.data.cache.CacheModule;
import io.ychescale9.weatherapp.data.repository.RepositoryModule;
import io.ychescale9.weatherapp.presentation.base.ActivityModule;
import io.ychescale9.weatherapp.ui.BaseUiTest;

/**
 * Created by yang on 14/12/16.
 */
@Singleton
@Component(
        modules = {
                TestAppModule.class,
                TestEnvironmentModule.class,
                TestTimeModule.class,
                TestConstantsModule.class,
                DataModule.class,
                RepositoryModule.class,
                TestApiModule.class,
                CacheModule.class,
                ActivityModule.class, // all activity sub-components
        }
)
public interface TestComponent extends AppComponent {

    void inject(BaseUiTest test);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder app(Application application);

        TestComponent build();
    }
}
