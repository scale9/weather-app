package io.ychescale9.weatherapp.util;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.NonNull;

/**
 * Created by yang on 13/12/16.
 */
public class MockLocationProvider {

    private final LocationManager locationManager;

    public MockLocationProvider(@NonNull LocationManager locationManager) {
        this.locationManager = locationManager;
    }


    public void addMockLocation(double latitude, double longitude) throws SecurityException {
        try {
            locationManager.addTestProvider(LocationManager.GPS_PROVIDER, false, false, false, false, false, true, true, Criteria.NO_REQUIREMENT, Criteria.NO_REQUIREMENT);
            locationManager.setTestProviderEnabled(LocationManager.GPS_PROVIDER, true);

            Location mockLocation = new Location(LocationManager.GPS_PROVIDER);
            mockLocation.setLatitude(latitude);
            mockLocation.setLongitude(longitude);
            mockLocation.setAltitude(0);
            mockLocation.setTime(System.currentTimeMillis());
            mockLocation.setAccuracy(1);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                // setting elapsed real time is required for API > 17
                mockLocation.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());
            }
            locationManager.setTestProviderLocation(LocationManager.GPS_PROVIDER, mockLocation);
        } catch (SecurityException e) {
            throw e;
        }
    }

    public void clear() {
        try {
            locationManager.removeTestProvider(LocationManager.GPS_PROVIDER);
        } catch (SecurityException e) {
            throw e;
        }
    }
}
