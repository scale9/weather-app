package io.ychescale9.weatherapp.util;

/**
 * Created by yang on 15/12/16.
 * A {@link Clock} implementation which supports overriding the return value of getCurrentTimeMillis() with a fixed value.
 * This can be used for timing-related tests.
 */
public class TestClock implements Clock {

    private long overriddenCurrentTimeMillis = -1;

    public void overrideCurrentTimeMillis(long overriddenCurrentTimeMillis) {
        this.overriddenCurrentTimeMillis = overriddenCurrentTimeMillis;
    }

    public void reset() {
        overriddenCurrentTimeMillis = -1;
    }

    @Override
    public long getCurrentTimeMillis() {
        // return current system time if overriddenCurrentTimeMillis < 0
        if (overriddenCurrentTimeMillis < 0) {
            return System.currentTimeMillis();
        } else {
            return overriddenCurrentTimeMillis;
        }
    }
}
