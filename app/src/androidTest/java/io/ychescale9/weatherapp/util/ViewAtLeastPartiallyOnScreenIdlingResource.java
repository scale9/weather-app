package io.ychescale9.weatherapp.util;

import android.graphics.Rect;
import android.support.test.espresso.IdlingResource;
import android.view.View;

/**
 * Created by yang on 15/12/16.
 * A implementation of {@link IdlingResource} that determines idleness by
 * checking whether the given view is at least partially visible on the screen
 */
public class ViewAtLeastPartiallyOnScreenIdlingResource implements IdlingResource {

    private final View view;
    private boolean idle;
    private volatile ResourceCallback mResourceCallback;

    public ViewAtLeastPartiallyOnScreenIdlingResource(final View view) {
        this.view = view;
        this.idle = false;
        this.mResourceCallback = null;
    }

    @Override
    public final String getName() {
        return ViewAtLeastPartiallyOnScreenIdlingResource.class.getSimpleName();
    }

    @Override
    public final boolean isIdleNow() {
        Rect rect = new Rect();
        boolean isFullyVisible = view == null || view.getGlobalVisibleRect(rect);

        idle = idle || isFullyVisible;

        if (idle) {
            if (mResourceCallback != null) {
                mResourceCallback.onTransitionToIdle();
            }
        }

        return idle;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
        mResourceCallback = resourceCallback;
    }
}