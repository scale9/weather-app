package io.ychescale9.weatherapp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.ychescale9.weatherapp.data.api.ApiConstants;
import io.ychescale9.weatherapp.data.api.TestApiConstants;
import io.ychescale9.weatherapp.util.TestUtil;

/**
 * Created by yang on 14/12/16.
 */
@Module
public class TestConstantsModule {

    @Provides
    @Singleton
    ApiConstants provideApiConstants() {
        return new TestApiConstants(TestUtil.MOCK_SERVER_PORT);
    }
}