package io.ychescale9.weatherapp.data.cache;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.observers.TestObserver;
import io.ychescale9.weatherapp.data.exception.CacheExpiredException;
import io.ychescale9.weatherapp.data.exception.CacheNotAvailableException;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.Temperature;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;
import io.ychescale9.weatherapp.data.model.WeatherType;
import io.ychescale9.weatherapp.util.Clock;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 11/12/16.
 * Unit tests for the implementation of {@link CurrentWeatherConditionCache}
 */
public class CurrentWeatherConditionCacheTest {

    // cache expiration time - 30 minutes
    private final int testExpirationTimeMilliseconds = 30 * 60 * 1000;

    private final Coordinates coordinates = new Coordinates(37.8136, 144.9631);

    private CurrentWeatherCondition dummyCurrentWeatherCondition;

    @Mock
    private Clock mockClock;

    private TestObserver<CurrentWeatherCondition> testObserver;

    private CurrentWeatherConditionCache currentWeatherConditionCache;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        dummyCurrentWeatherCondition = new CurrentWeatherCondition(
                1481351400000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 23),
                1481309446000L,
                1481362487000L);

        testObserver = new TestObserver<>();
        currentWeatherConditionCache = new CurrentWeatherConditionCache(
                testExpirationTimeMilliseconds, mockClock);

        // mock clock returns the real currentTimeMillis by default
        when(mockClock.getCurrentTimeMillis()).thenReturn(System.currentTimeMillis());
    }

    @Test
    public void load_cacheNotAvailable() throws Exception {
        // no data has been written to the cache
        // load cache
        currentWeatherConditionCache.load(coordinates).subscribe(testObserver);

        // verify that the correct exception is thrown
        testObserver.assertError(CacheNotAvailableException.class);
        testObserver.assertNoValues();
    }

    @Test
    public void load_incorrectCoordinates() throws Exception {
        // put some non-empty data into the cache
        currentWeatherConditionCache.update(coordinates, dummyCurrentWeatherCondition);
        // load cache with a new coordinates
        Coordinates newCoordinates = new Coordinates(50.8136, 120.9631);
        currentWeatherConditionCache.load(newCoordinates).subscribe(testObserver);

        // verify that the correct exception is thrown
        testObserver.assertError(CacheNotAvailableException.class);
        testObserver.assertNoValues();
    }

    @Test
    public void load_validCache() throws Exception {
        // put some non-empty data into the cache
        currentWeatherConditionCache.update(coordinates, dummyCurrentWeatherCondition);
        // load cache
        currentWeatherConditionCache.load(coordinates).subscribe(testObserver);
        testObserver.assertNoErrors();

        // verify that the correct cache has been loaded
        testObserver.assertValue(dummyCurrentWeatherCondition);
    }

    @Test
    public void load_cacheExpired() throws Exception {
        // put some non-empty data into the cache
        currentWeatherConditionCache.update(coordinates, dummyCurrentWeatherCondition);
        // advance the clock so the cache is expired
        when(mockClock.getCurrentTimeMillis())
                .thenReturn(System.currentTimeMillis() + testExpirationTimeMilliseconds + 50);

        // load cache
        currentWeatherConditionCache.load(coordinates).subscribe(testObserver);

        // verify that the correct exception is thrown
        testObserver.assertError(CacheExpiredException.class);
        testObserver.assertNoValues();
    }

    @Test
    public void clear() throws Exception {
        // put some non-empty data into the cache
        currentWeatherConditionCache.update(coordinates, dummyCurrentWeatherCondition);
        // clear the cache
        currentWeatherConditionCache.clear();
        // load cache
        currentWeatherConditionCache.load(coordinates).subscribe(testObserver);

        // verify that the correct exception is thrown
        testObserver.assertError(CacheNotAvailableException.class);
        testObserver.assertNoValues();
    }

    @Test
    public void isExpired_true() throws Exception {
        // put some non-empty data into the cache
        currentWeatherConditionCache.update(coordinates, dummyCurrentWeatherCondition);
        // advance the clock so the cache is expired
        when(mockClock.getCurrentTimeMillis())
                .thenReturn(System.currentTimeMillis() + testExpirationTimeMilliseconds + 50);

        // verify that the cache has expired
        assertThat(currentWeatherConditionCache.isExpired(), is(true));
    }

    @Test
    public void isExpired_false() throws Exception {
        // put some non-empty data into the cache
        currentWeatherConditionCache.update(coordinates, dummyCurrentWeatherCondition);
        // advance the clock to some time BEFORE the cache is expired
        when(mockClock.getCurrentTimeMillis())
                .thenReturn(System.currentTimeMillis() + testExpirationTimeMilliseconds - 50);

        // verify that the cache has not expired
        assertThat(currentWeatherConditionCache.isExpired(), is(false));
    }
}