package io.ychescale9.weatherapp.data.cache;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.TestObserver;
import io.ychescale9.weatherapp.data.exception.CacheExpiredException;
import io.ychescale9.weatherapp.data.exception.CacheNotAvailableException;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.SummaryForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.Temperature;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;
import io.ychescale9.weatherapp.data.model.WeatherType;
import io.ychescale9.weatherapp.util.Clock;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 11/12/16.
 * Unit tests for the implementation of {@link DailyForecastWeatherConditionsCache}
 */
public class DailyForecastWeatherConditionsCacheTest {

    // cache expiration time - 3 hours
    private final int testExpirationTimeMilliseconds = 3 * 60 * 60 * 1000;

    private final Coordinates coordinates = new Coordinates(37.8136, 144.9631);

    private List<SummaryForecastWeatherCondition> dummyDailyForecastWeatherConditions;

    @Mock
    private Clock mockClock;

    private TestObserver<List<SummaryForecastWeatherCondition>> testObserver;

    private DailyForecastWeatherConditionsCache dailyForecastWeatherConditionsCache;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        dummyDailyForecastWeatherConditions = new ArrayList<>();
        dummyDailyForecastWeatherConditions.add(new SummaryForecastWeatherCondition(
                1481371400000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 28),
                new Temperature(TemperatureUnit.CELSIUS, 12)
        ));
        dummyDailyForecastWeatherConditions.add(new SummaryForecastWeatherCondition(
                1481371500000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 26),
                new Temperature(TemperatureUnit.CELSIUS, 13)
        ));
        dummyDailyForecastWeatherConditions.add(new SummaryForecastWeatherCondition(
                1481371800000L,
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 30),
                new Temperature(TemperatureUnit.CELSIUS, 14)
        ));

        testObserver = new TestObserver<>();
        dailyForecastWeatherConditionsCache = new DailyForecastWeatherConditionsCache(
                testExpirationTimeMilliseconds, mockClock);

        // mock clock returns the real currentTimeMillis by default
        when(mockClock.getCurrentTimeMillis()).thenReturn(System.currentTimeMillis());
    }

    @Test
    public void load_cacheNotAvailable() throws Exception {
        // no data has been written to the cache
        // load cache
        dailyForecastWeatherConditionsCache.load(coordinates).subscribe(testObserver);

        // verify that the correct exception is thrown
        testObserver.assertError(CacheNotAvailableException.class);
        testObserver.assertNoValues();
    }

    @Test
    public void load_incorrectCoordinates() throws Exception {
        // put some non-empty data into the cache
        dailyForecastWeatherConditionsCache.update(coordinates, dummyDailyForecastWeatherConditions);
        // load cache with a new coordinates
        Coordinates newCoordinates = new Coordinates(50.8136, 120.9631);
        dailyForecastWeatherConditionsCache.load(newCoordinates).subscribe(testObserver);

        // verify that the correct exception is thrown
        testObserver.assertError(CacheNotAvailableException.class);
        testObserver.assertNoValues();
    }

    @Test
    public void load_validCache() throws Exception {
        // put some non-empty data into the cache
        dailyForecastWeatherConditionsCache.update(coordinates, dummyDailyForecastWeatherConditions);
        // load cache
        dailyForecastWeatherConditionsCache.load(coordinates).subscribe(testObserver);
        testObserver.assertNoErrors();

        // verify that the correct cache has been loaded
        testObserver.assertValue(dummyDailyForecastWeatherConditions);
    }

    @Test
    public void load_cacheExpired() throws Exception {
        // put some non-empty data into the cache
        dailyForecastWeatherConditionsCache.update(coordinates, dummyDailyForecastWeatherConditions);
        // advance the clock so the cache is expired
        when(mockClock.getCurrentTimeMillis())
                .thenReturn(System.currentTimeMillis() + testExpirationTimeMilliseconds + 50);

        // load cache
        dailyForecastWeatherConditionsCache.load(coordinates).subscribe(testObserver);

        // verify that the correct exception is thrown
        testObserver.assertError(CacheExpiredException.class);
        testObserver.assertNoValues();
    }

    @Test
    public void update_withEmptyData() throws Exception {
        // put some empty data into the cache
        dailyForecastWeatherConditionsCache.update(coordinates, new ArrayList<SummaryForecastWeatherCondition>());
        // load cache
        dailyForecastWeatherConditionsCache.load(coordinates).subscribe(testObserver);

        // verify that the correct exception is thrown
        testObserver.assertError(CacheNotAvailableException.class);
        testObserver.assertNoValues();
    }

    @Test
    public void clear() throws Exception {
        // put some non-empty data into the cache
        dailyForecastWeatherConditionsCache.update(coordinates, dummyDailyForecastWeatherConditions);
        // clear the cache
        dailyForecastWeatherConditionsCache.clear();
        // load cache
        dailyForecastWeatherConditionsCache.load(coordinates).subscribe(testObserver);

        // verify that the correct exception is thrown
        testObserver.assertError(CacheNotAvailableException.class);
        testObserver.assertNoValues();
    }

    @Test
    public void isExpired_true() throws Exception {
        // put some non-empty data into the cache
        dailyForecastWeatherConditionsCache.update(coordinates, dummyDailyForecastWeatherConditions);
        // advance the clock so the cache is expired
        when(mockClock.getCurrentTimeMillis())
                .thenReturn(System.currentTimeMillis() + testExpirationTimeMilliseconds + 50);

        // verify that the cache has expired
        assertThat(dailyForecastWeatherConditionsCache.isExpired(), is(true));
    }

    @Test
    public void isExpired_false() throws Exception {
        // put some non-empty data into the cache
        dailyForecastWeatherConditionsCache.update(coordinates, dummyDailyForecastWeatherConditions);
        // advance the clock to some time BEFORE the cache is expired
        when(mockClock.getCurrentTimeMillis())
                .thenReturn(System.currentTimeMillis() + testExpirationTimeMilliseconds - 50);

        // verify that the cache has expired
        assertThat(dailyForecastWeatherConditionsCache.isExpired(), is(false));
    }
}