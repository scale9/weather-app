package io.ychescale9.weatherapp.data.repository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.ychescale9.weatherapp.data.api.WeatherAppService;
import io.ychescale9.weatherapp.data.cache.CurrentWeatherConditionCache;
import io.ychescale9.weatherapp.data.cache.DailyForecastWeatherConditionsCache;
import io.ychescale9.weatherapp.data.exception.CacheExpiredException;
import io.ychescale9.weatherapp.data.exception.CacheNotAvailableException;
import io.ychescale9.weatherapp.data.helper.WeatherDataTransformer;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.ForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.SummaryForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.Temperature;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;
import io.ychescale9.weatherapp.data.model.WeatherType;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 11/12/16.
 * Unit tests for the implementation of {@link WeatherAppRepositoryImpl}
 */
@SuppressWarnings("unchecked")
public class WeatherAppRepositoryImplTest {

    private final Coordinates coordinates = new Coordinates(37.8136, 144.9631);

    private CurrentWeatherCondition cacheCurrentWeatherCondition;
    private CurrentWeatherCondition remoteCurrentWeatherCondition;

    private List<SummaryForecastWeatherCondition> cacheDailyForecastWeatherConditions;
    private List<ForecastWeatherCondition> remoteForecastWeatherConditions;
    private List<SummaryForecastWeatherCondition> remoteDailyForecastWeatherConditions;

    @Mock
    private CurrentWeatherConditionCache mockCurrentWeatherConditionCache;

    @Mock
    private DailyForecastWeatherConditionsCache mockDailyForecastWeatherConditionsCache;

    @Mock
    private WeatherAppService mockWeatherAppService;

    @Mock
    private WeatherDataTransformer mockWeatherDataTransformer;

    private TestObserver testObserver;

    private WeatherAppRepository weatherAppRepository;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        cacheCurrentWeatherCondition = new CurrentWeatherCondition(
                1481351400000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 23),
                1481309446000L,
                1481362487000L);

        remoteCurrentWeatherCondition = new CurrentWeatherCondition(
                1481351600L,
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 25),
                1481309446000L,
                1481362487000L);

        cacheDailyForecastWeatherConditions = new ArrayList<>();
        cacheDailyForecastWeatherConditions.add(new SummaryForecastWeatherCondition(
                1481371400000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 28),
                new Temperature(TemperatureUnit.CELSIUS, 12)
        ));
        cacheDailyForecastWeatherConditions.add(new SummaryForecastWeatherCondition(
                1481371500000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 26),
                new Temperature(TemperatureUnit.CELSIUS, 13)
        ));
        cacheDailyForecastWeatherConditions.add(new SummaryForecastWeatherCondition(
                1481371800000L,
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 30),
                new Temperature(TemperatureUnit.CELSIUS, 14)
        ));

        remoteForecastWeatherConditions = new ArrayList<>();
        remoteForecastWeatherConditions.add(new ForecastWeatherCondition(
                1481371400000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 23)
        ));
        remoteForecastWeatherConditions.add(new ForecastWeatherCondition(
                1481371500000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 20)
        ));
        remoteForecastWeatherConditions.add(new ForecastWeatherCondition(
                1481371800000L,
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 24)
        ));
        remoteForecastWeatherConditions.add(new ForecastWeatherCondition(
                1481372000000L,
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 25)
        ));
        remoteForecastWeatherConditions.add(new ForecastWeatherCondition(
                1481372300000L,
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 22)
        ));
        remoteForecastWeatherConditions.add(new ForecastWeatherCondition(
                1481372400000L,
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 20)
        ));

        remoteDailyForecastWeatherConditions = new ArrayList<>();
        remoteDailyForecastWeatherConditions.add(new SummaryForecastWeatherCondition(
                1481371400000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 28),
                new Temperature(TemperatureUnit.CELSIUS, 12)
        ));
        remoteDailyForecastWeatherConditions.add(new SummaryForecastWeatherCondition(
                1481371500000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 26),
                new Temperature(TemperatureUnit.CELSIUS, 13)
        ));
        remoteDailyForecastWeatherConditions.add(new SummaryForecastWeatherCondition(
                1481371800000L,
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 30),
                new Temperature(TemperatureUnit.CELSIUS, 14)
        ));

        testObserver = new TestObserver();
        weatherAppRepository = new WeatherAppRepositoryImpl(
                mockCurrentWeatherConditionCache,
                mockDailyForecastWeatherConditionsCache,
                mockWeatherAppService,
                mockWeatherDataTransformer);
    }

    @Test
    public void loadCurrentWeatherCondition_noCache() throws Exception {
        // given there's no cache data
        when(mockCurrentWeatherConditionCache.load(any(Coordinates.class))).thenReturn(Observable.<CurrentWeatherCondition>error(new CacheNotAvailableException()));
        // given valid remote data
        when(mockWeatherAppService.loadCurrentWeatherCondition(coordinates.getLatitude(), coordinates.getLongitude())).thenReturn(Observable.just(remoteCurrentWeatherCondition));
        // given transformed current weather conditions
        when(mockWeatherDataTransformer.correctCurrentDateTime(any(CurrentWeatherCondition.class))).thenReturn(remoteCurrentWeatherCondition);

        weatherAppRepository.loadCurrentWeatherCondition(coordinates).subscribe(testObserver);
        testObserver.assertNoErrors();

        // should update cache with loaded data from server
        verify(mockCurrentWeatherConditionCache).update(eq(coordinates), any(CurrentWeatherCondition.class));

        // verify that the correct data has been loaded from server
        testObserver.assertValue(remoteCurrentWeatherCondition);
    }

    @Test
    public void loadCurrentWeatherCondition_hasValidCache() throws Exception {
        // given cache data
        when(mockCurrentWeatherConditionCache.load(any(Coordinates.class))).thenReturn(Observable.just(cacheCurrentWeatherCondition));
        // given valid remote data
        when(mockWeatherAppService.loadCurrentWeatherCondition(coordinates.getLatitude(), coordinates.getLongitude())).thenReturn(Observable.just(remoteCurrentWeatherCondition));

        weatherAppRepository.loadCurrentWeatherCondition(coordinates).subscribe(testObserver);
        testObserver.assertNoErrors();

        // should NOT update cache
        verify(mockCurrentWeatherConditionCache, never()).update(coordinates, remoteCurrentWeatherCondition);

        // verify that the correct data has been loaded from cache
        testObserver.assertValue(cacheCurrentWeatherCondition);
    }

    @Test
    public void loadCurrentWeatherCondition_hasExpiredCache() throws Exception {
        // given cache has expired
        when(mockCurrentWeatherConditionCache.load(any(Coordinates.class))).thenReturn(Observable.<CurrentWeatherCondition>error(new CacheExpiredException()));
        // given valid remote data
        when(mockWeatherAppService.loadCurrentWeatherCondition(coordinates.getLatitude(), coordinates.getLongitude())).thenReturn(Observable.just(remoteCurrentWeatherCondition));
        // given transformed current weather conditions
        when(mockWeatherDataTransformer.correctCurrentDateTime(any(CurrentWeatherCondition.class))).thenReturn(remoteCurrentWeatherCondition);

        weatherAppRepository.loadCurrentWeatherCondition(coordinates).subscribe(testObserver);
        testObserver.assertNoErrors();

        // should try to load from cache
        verify(mockCurrentWeatherConditionCache).load(coordinates);
        // should then try to load from server
        verify(mockWeatherAppService).loadCurrentWeatherCondition(coordinates.getLatitude(), coordinates.getLongitude());
        // should then update cache with loaded data from server
        verify(mockCurrentWeatherConditionCache).update(coordinates, remoteCurrentWeatherCondition);

        // verify that the correct data has been loaded from cache
        testObserver.assertValue(remoteCurrentWeatherCondition);
    }

    @Test
    public void loadCurrentWeatherCondition_noCache_errorLoadingDataFromServer() throws Exception {
        // given there's no cache data
        when(mockCurrentWeatherConditionCache.load(any(Coordinates.class))).thenReturn(Observable.<CurrentWeatherCondition>error(new CacheNotAvailableException()));
        // given remote service returns an error
        when(mockWeatherAppService.loadCurrentWeatherCondition(coordinates.getLatitude(), coordinates.getLongitude())).thenReturn(Observable.<CurrentWeatherCondition>error(new Exception("server error")));

        weatherAppRepository.loadCurrentWeatherCondition(coordinates).subscribe(testObserver);
        testObserver.assertError(Exception.class);

        // should try to load from cache
        verify(mockCurrentWeatherConditionCache).load(coordinates);
        // should then try to load from server
        verify(mockWeatherAppService).loadCurrentWeatherCondition(coordinates.getLatitude(), coordinates.getLongitude());
        // should NOT try to transform current weather condition
        verify(mockWeatherDataTransformer, never()).correctCurrentDateTime(any(CurrentWeatherCondition.class));
        // should NOT update cache
        verify(mockCurrentWeatherConditionCache, never()).update(coordinates, remoteCurrentWeatherCondition);
    }

    @Test
    public void loadDailyForecastWeatherConditions_noCache() throws Exception {
        // given there's no cache data
        when(mockDailyForecastWeatherConditionsCache.load(any(Coordinates.class))).thenReturn(Observable.<List<SummaryForecastWeatherCondition>>error(new CacheNotAvailableException()));
        // given valid remote data
        when(mockWeatherAppService.loadForecastWeatherConditions(coordinates.getLatitude(), coordinates.getLongitude())).thenReturn(Observable.just(remoteForecastWeatherConditions));
        // given transformed daily forecast weather conditions
        when(mockWeatherDataTransformer.toDailySummary(ArgumentMatchers.<ForecastWeatherCondition>anyList())).thenReturn(remoteDailyForecastWeatherConditions);

        weatherAppRepository.loadDailyForecastWeatherConditions(coordinates).subscribe(testObserver);
        testObserver.assertNoErrors();

        // should update cache with loaded data from server
        verify(mockDailyForecastWeatherConditionsCache).update(eq(coordinates), ArgumentMatchers.<SummaryForecastWeatherCondition>anyList());

        // verify that the correct data has been loaded from server
        testObserver.assertValue(remoteDailyForecastWeatherConditions);
    }

    @Test
    public void loadDailyForecastWeatherConditions_hasValidCache() throws Exception {
        // given cache data
        when(mockDailyForecastWeatherConditionsCache.load(any(Coordinates.class))).thenReturn(Observable.just(cacheDailyForecastWeatherConditions));
        // given valid remote data
        when(mockWeatherAppService.loadForecastWeatherConditions(coordinates.getLatitude(), coordinates.getLongitude())).thenReturn(Observable.just(remoteForecastWeatherConditions));
        // given transformed daily forecast weather conditions
        when(mockWeatherDataTransformer.toDailySummary(ArgumentMatchers.<ForecastWeatherCondition>anyList())).thenReturn(remoteDailyForecastWeatherConditions);

        weatherAppRepository.loadDailyForecastWeatherConditions(coordinates).subscribe(testObserver);
        testObserver.assertNoErrors();

        // should NOT update cache
        verify(mockDailyForecastWeatherConditionsCache, never()).update(eq(coordinates), ArgumentMatchers.<SummaryForecastWeatherCondition>anyList());

        // verify that the correct data has been loaded from server
        testObserver.assertValue(cacheDailyForecastWeatherConditions);
    }

    @Test
    public void loadDailyForecastWeatherConditions_hasExpiredCache() throws Exception {
        // given cache has expired
        when(mockDailyForecastWeatherConditionsCache.load(any(Coordinates.class))).thenReturn(Observable.<List<SummaryForecastWeatherCondition>>error(new CacheExpiredException()));
        // given valid remote data
        when(mockWeatherAppService.loadForecastWeatherConditions(coordinates.getLatitude(), coordinates.getLongitude())).thenReturn(Observable.just(remoteForecastWeatherConditions));
        // given transformed daily forecast weather conditions
        when(mockWeatherDataTransformer.toDailySummary(ArgumentMatchers.<ForecastWeatherCondition>anyList())).thenReturn(remoteDailyForecastWeatherConditions);

        weatherAppRepository.loadDailyForecastWeatherConditions(coordinates).subscribe(testObserver);
        testObserver.assertNoErrors();

        // should try to load from cache
        verify(mockDailyForecastWeatherConditionsCache).load(coordinates);
        // should then try to load from server
        verify(mockWeatherAppService).loadForecastWeatherConditions(coordinates.getLatitude(), coordinates.getLongitude());
        // should then update cache with loaded data from server
        verify(mockDailyForecastWeatherConditionsCache).update(coordinates, remoteDailyForecastWeatherConditions);

        // verify that the correct data has been loaded from server
        testObserver.assertValue(remoteDailyForecastWeatherConditions);
    }

    @Test
    public void loadDailyForecastWeatherConditions_noCache_errorLoadingDataFromServer() throws Exception {
        // given there's no cache data
        when(mockDailyForecastWeatherConditionsCache.load(any(Coordinates.class))).thenReturn(Observable.<List<SummaryForecastWeatherCondition>>error(new CacheNotAvailableException()));
        // given remote service returns an error
        when(mockWeatherAppService.loadForecastWeatherConditions(coordinates.getLatitude(), coordinates.getLongitude())).thenReturn(Observable.<List<ForecastWeatherCondition>>error(new Exception("server error")));

        weatherAppRepository.loadDailyForecastWeatherConditions(coordinates).subscribe(testObserver);
        testObserver.assertError(Exception.class);

        // should try to load from cache
        verify(mockDailyForecastWeatherConditionsCache).load(coordinates);
        // should then try to load from server
        verify(mockWeatherAppService).loadForecastWeatherConditions(coordinates.getLatitude(), coordinates.getLongitude());
        // should NOT try to transform to daily forecast weather conditions
        verify(mockWeatherDataTransformer, never()).toDailySummary(ArgumentMatchers.<ForecastWeatherCondition>anyList());
        // should NOT update cache
        verify(mockDailyForecastWeatherConditionsCache, never()).update(coordinates, remoteDailyForecastWeatherConditions);
    }
}