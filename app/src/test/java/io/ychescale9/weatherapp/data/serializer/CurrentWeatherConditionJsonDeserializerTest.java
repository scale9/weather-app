package io.ychescale9.weatherapp.data.serializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import org.junit.Before;
import org.junit.Test;

import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;

/**
 * Created by yang on 10/12/16.
 * Unit tests for the implementation of {@link CurrentWeatherConditionJsonDeserializer}
 */
public class CurrentWeatherConditionJsonDeserializerTest {

    final String DUMMY_CURRENT_WEATHER_CONDITION = "{\"coord\":{\"lon\":145.07,\"lat\":-37.89},\"weather\":[{\"id\":801,\"main\":\"Clouds\",\"description\":\"few clouds\",\"icon\":\"02d\"}],\"base\":\"stations\",\"main\":{\"temp\":18,\"pressure\":1022,\"humidity\":52,\"temp_min\":18,\"temp_max\":18},\"visibility\":10000,\"wind\":{\"speed\":6.7,\"deg\":140},\"clouds\":{\"all\":20},\"dt\":1481351400,\"sys\":{\"type\":1,\"id\":8201,\"message\":0.011,\"country\":\"AU\",\"sunrise\":1481309446,\"sunset\":1481362487},\"id\":7932639,\"name\":\"Murrumbeena\",\"cod\":200}";

    private Gson gson;

    @Before
    public void setUp() {
        // create Gson object using CurrentWeatherConditionJsonDeserializer
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(CurrentWeatherCondition.class, new CurrentWeatherConditionJsonDeserializer());
        gson = gsonBuilder.create();
    }

    @Test
    public void deserializeCurrentWeatherConditions_happyCase() throws Exception {
        CurrentWeatherCondition currentWeatherCondition = gson.fromJson(DUMMY_CURRENT_WEATHER_CONDITION, CurrentWeatherCondition.class);

        assertNotNull(currentWeatherCondition);
        assertNotNull(currentWeatherCondition.currentTemperature);
        assertThat(currentWeatherCondition.currentTemperature.getDegrees(), is(18.0));
        assertThat(currentWeatherCondition.currentTemperature.getUnit(), is(TemperatureUnit.CELSIUS));
        assertThat(currentWeatherCondition.dateTimeMillis, is(1481351400000L));
        assertNotNull(currentWeatherCondition.weatherType);
        assertThat(currentWeatherCondition.weatherType.getWeatherId(), is(801));
        assertThat(currentWeatherCondition.weatherType.getShortDescription(), equalToIgnoringCase("Clouds"));
        assertThat(currentWeatherCondition.weatherType.getLongDescription(), equalToIgnoringCase("few clouds"));
        assertThat(currentWeatherCondition.sunriseDateTimeMillis, is(1481309446000L));
        assertThat(currentWeatherCondition.sunsetDateTimeMillis, is(1481362487000L));
    }

    @Test(expected = JsonSyntaxException.class)
    public void deserializeCurrentWeatherConditions_invalidJson() throws Exception {
        gson.fromJson("{invalid json}", CurrentWeatherCondition.class);
    }
}