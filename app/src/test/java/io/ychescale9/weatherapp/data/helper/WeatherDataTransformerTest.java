package io.ychescale9.weatherapp.data.helper;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.ychescale9.weatherapp.TimeZoneUtcRule;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.ForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.SummaryForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.Temperature;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;
import io.ychescale9.weatherapp.data.model.WeatherType;
import io.ychescale9.weatherapp.util.Clock;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 11/12/16.
 * Unit tests for the implementation of {@link WeatherDataTransformer}
 */
public class WeatherDataTransformerTest {

    @Rule
    public TimeZoneUtcRule timeZoneUtcRule = new TimeZoneUtcRule();

    @Mock
    private Clock mockClock;

    private WeatherDataTransformer weatherDataTransformer;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        weatherDataTransformer = new WeatherDataTransformer(mockClock);

        when(mockClock.getCurrentTimeMillis())
                .thenReturn(1481446800000L); // 2016-12-11 09:00:00
    }

    @Test
    public void correctCurrentTime() throws Exception {
        CurrentWeatherCondition currentWeatherCondition = new CurrentWeatherCondition(
                1481437800000L, // 2016-12-11 06:30:00
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 23),
                1481309446000L,
                1481362487000L);

        currentWeatherCondition = weatherDataTransformer.correctCurrentDateTime(currentWeatherCondition);
        assertThat(currentWeatherCondition.dateTimeMillis, is(mockClock.getCurrentTimeMillis()));
    }

    @Test
    public void toDaily() throws Exception {
        List<ForecastWeatherCondition> forecastWeatherConditions = new ArrayList<>();
        // day 1 (today)
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481457600000L, // 2016-12-11 12:00:00
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 10.57)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481468400000L, // 2016-12-11 15:00:00
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 7.67)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481479200000L, // 2016-12-11 18:00:00
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 6.22)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481490000000L, // 2016-12-11 21:00:00
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 12.54)
        ));
        // day 2
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481500800000L, // 2016-12-12 00:00:00
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 22.33)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481511600000L, // 2016-12-12 03:00:00
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 25.5)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481522400000L, // 2016-12-12 06:00:00
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 26.46)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481533200000L, // 2016-12-12 09:00:00
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 23.39)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481544000000L, // 2016-12-12 12:00:00
                new WeatherType(802, "Clouds", "scattered clouds"),
                new Temperature(TemperatureUnit.CELSIUS, 18.68)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481554800000L, // 2016-12-12 15:00:00
                new WeatherType(801, "Clouds", "few clouds"),
                new Temperature(TemperatureUnit.CELSIUS, 19.1)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481565600000L, // 2016-12-12 18:00:00
                new WeatherType(803, "Clouds", "broken clouds"),
                new Temperature(TemperatureUnit.CELSIUS, 19.01)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481576400000L, // 2016-12-12 21:00:00
                new WeatherType(801, "Clouds", "few clouds"),
                new Temperature(TemperatureUnit.CELSIUS, 21.71)
        ));
        // day 3
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481587200000L, // 2016-12-13 00:00:00
                new WeatherType(804, "Clouds", "overcast clouds"),
                new Temperature(TemperatureUnit.CELSIUS, 24.98)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481598000000L, // 2016-12-13 03:00:00
                new WeatherType(804, "Clouds", "overcast clouds"),
                new Temperature(TemperatureUnit.CELSIUS, 26.5)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481608800000L, // 2016-12-13 06:00:00
                new WeatherType(500, "Rain", "light rain"),
                new Temperature(TemperatureUnit.CELSIUS, 23.76)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481619600000L, // 2016-12-13 09:00:00
                new WeatherType(804, "Clouds", "overcast clouds"),
                new Temperature(TemperatureUnit.CELSIUS, 23.69)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481630400000L, // 2016-12-13 12:00:00
                new WeatherType(803, "Clouds", "broken clouds"),
                new Temperature(TemperatureUnit.CELSIUS, 22.41)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481641200000L, // 2016-12-13 15:00:00
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 18.57)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481652000000L, // 2016-12-13 18:00:00
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 14.65)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481662800000L, // 2016-12-13 21:00:00
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 14.66)
        ));

        List<SummaryForecastWeatherCondition> dailyForecastWeatherConditions = weatherDataTransformer.toDailySummary(forecastWeatherConditions);

        // forecasts are grouped into 3 days
        assertThat(dailyForecastWeatherConditions.size(), is(3));
        // day 1 (today)
        assertThat(dailyForecastWeatherConditions.get(0).dateTimeMillis, is(1481414400000L));
        assertThat(dailyForecastWeatherConditions.get(0).weatherType.getWeatherId(), is(800));
        assertThat(dailyForecastWeatherConditions.get(0).forecastMaxTemperature.getDegrees(), is(12.54));
        assertThat(dailyForecastWeatherConditions.get(0).forecastMinTemperature.getDegrees(), is(6.22));
        // day 2
        assertThat(dailyForecastWeatherConditions.get(1).dateTimeMillis, is(1481500800000L));
        assertThat(dailyForecastWeatherConditions.get(1).weatherType.getWeatherId(), is(800));
        assertThat(dailyForecastWeatherConditions.get(1).forecastMaxTemperature.getDegrees(), is(26.46));
        assertThat(dailyForecastWeatherConditions.get(1).forecastMinTemperature.getDegrees(), is(18.68));
        // day 3
        assertThat(dailyForecastWeatherConditions.get(2).dateTimeMillis, is(1481587200000L));
        assertThat(dailyForecastWeatherConditions.get(2).weatherType.getWeatherId(), is(804));
        assertThat(dailyForecastWeatherConditions.get(2).forecastMaxTemperature.getDegrees(), is(26.5));
        assertThat(dailyForecastWeatherConditions.get(2).forecastMinTemperature.getDegrees(), is(14.65));
    }

    @Test
    public void toDaily_weatherTypeIsMostCommonDuringTheDay() throws Exception {
        List<ForecastWeatherCondition> forecastWeatherConditions = new ArrayList<>();
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481457600000L, // 2016-12-11 12:00:00
                new WeatherType(500, "Rain", "light rain"),
                new Temperature(TemperatureUnit.CELSIUS, 10.57)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481468400000L, // 2016-12-11 15:00:00
                new WeatherType(600, "Snow", "light snow"),
                new Temperature(TemperatureUnit.CELSIUS, 7.67)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481479200000L, // 2016-12-11 18:00:00
                new WeatherType(601, "Snow", "snow"),
                new Temperature(TemperatureUnit.CELSIUS, 6.22)
        ));
        forecastWeatherConditions.add(new ForecastWeatherCondition(
                1481490000000L, // 2016-12-11 21:00:00
                new WeatherType(301, "Drizzle", "drizzle"),
                new Temperature(TemperatureUnit.CELSIUS, 12.54)
        ));

        List<SummaryForecastWeatherCondition> dailyForecastWeatherConditions = weatherDataTransformer.toDailySummary(forecastWeatherConditions);

        assertThat(dailyForecastWeatherConditions.size(), is(1));
        assertThat(dailyForecastWeatherConditions.get(0).weatherType.getWeatherId(), is(600));
        assertThat(dailyForecastWeatherConditions.get(0).weatherType.getShortDescription(), is("Snow"));
        assertThat(dailyForecastWeatherConditions.get(0).weatherType.getLongDescription(), is("light snow"));
    }
}