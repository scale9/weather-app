package io.ychescale9.weatherapp.data.model;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.is;

/**
 * Created by yang on 10/12/16.
 * Unit tests for the implementation of {@link Temperature}
 */
public class TemperatureTest {

    // the error range allowed for unit conversion
    private final static double conversionErrorRange = 0.1;

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void celsiusToFahrenheit() throws Exception {
        Temperature celsiusTemperature = new Temperature(TemperatureUnit.CELSIUS, 23.5);
        Temperature fahrenheitTemperature = celsiusTemperature.toFahrenheit();
        assertThat(fahrenheitTemperature.getDegrees(), closeTo(74.3, conversionErrorRange));
    }

    @Test
    public void celsiusToCelsius() throws Exception {
        Temperature celsiusTemperature = new Temperature(TemperatureUnit.CELSIUS, 23.5);
        Temperature celsiusTemperature2 = celsiusTemperature.toCelsius();
        assertThat(celsiusTemperature, is(celsiusTemperature2));
    }

    @Test
    public void fahrenheitToCelsius() throws Exception {
        Temperature fahrenheitTemperature = new Temperature(TemperatureUnit.FAHRENHEIT, 64);
        Temperature celsiusTemperature = fahrenheitTemperature.toCelsius();
        assertThat(celsiusTemperature.getDegrees(), closeTo(17.78, conversionErrorRange));
    }

    @Test
    public void fahrenheitsToFahrenheit() throws Exception {
        Temperature fahrenheitTemperature = new Temperature(TemperatureUnit.FAHRENHEIT, 64);
        Temperature fahrenheitTemperature2 = fahrenheitTemperature.toFahrenheit();
        assertThat(fahrenheitTemperature, is(fahrenheitTemperature2));
    }

    @Test
    public void getFullStringCelsius() throws Exception {
        Temperature temperature = new Temperature(TemperatureUnit.CELSIUS, 23.5);
        assertThat(temperature.getFullString(), is("24°C"));
    }

    @Test
    public void getFullStringFahrenheit() throws Exception {
        Temperature temperature = new Temperature(TemperatureUnit.FAHRENHEIT, 64.4);
        assertThat(temperature.getFullString(), is("64°F"));
    }

    @Test
    public void getShortStringCelsius() throws Exception {
        Temperature temperature = new Temperature(TemperatureUnit.CELSIUS, 23.5);
        assertThat(temperature.getShortString(), is("24°"));
    }

    @Test
    public void getShortStringFahrenheit() throws Exception {
        Temperature temperature = new Temperature(TemperatureUnit.FAHRENHEIT, 64.4);
        assertThat(temperature.getShortString(), is("64°"));
    }

}