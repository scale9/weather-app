package io.ychescale9.weatherapp.data.serializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import org.junit.Before;
import org.junit.Test;

import io.ychescale9.weatherapp.data.model.ForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;

/**
 * Created by yang on 10/12/16.
 * Unit tests for the implementation of {@link ForecastWeatherConditionJsonDeserializer}
 */
public class ForecastWeatherConditionJsonDeserializerTest {

    final String DUMMY_FORECAST_WEATHER_CONDITION = "{\"dt\":1481306400,\"main\":{\"temp\":11.52,\"temp_min\":11.52,\"temp_max\":11.84,\"pressure\":1017.37,\"sea_level\":1035.49,\"grnd_level\":1017.37,\"humidity\":82,\"temp_kf\":-0.32},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"light rain\",\"icon\":\"10n\"}],\"clouds\":{\"all\":92},\"wind\":{\"speed\":1.22,\"deg\":237.502},\"rain\":{\"3h\":0.04},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2016-12-09 18:00:00\"}";

    private Gson gson;

    @Before
    public void setUp() {
        // create Gson object using ForecastWeatherConditionJsonDeserializer
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(ForecastWeatherCondition.class, new ForecastWeatherConditionJsonDeserializer());
        gson = gsonBuilder.create();
    }

    @Test
    public void deserializeForecastWeatherConditions_happyCase() throws Exception {
        ForecastWeatherCondition forecastWeatherCondition = gson.fromJson(DUMMY_FORECAST_WEATHER_CONDITION, ForecastWeatherCondition.class);

        assertNotNull(forecastWeatherCondition);
        assertNotNull(forecastWeatherCondition.forecastTemperature);
        assertThat(forecastWeatherCondition.forecastTemperature.getDegrees(), is(11.52));
        assertThat(forecastWeatherCondition.forecastTemperature.getUnit(), is(TemperatureUnit.CELSIUS));
        assertThat(forecastWeatherCondition.dateTimeMillis, is(1481306400000L));
        assertNotNull(forecastWeatherCondition.weatherType);
        assertThat(forecastWeatherCondition.weatherType.getWeatherId(), is(500));
        assertThat(forecastWeatherCondition.weatherType.getShortDescription(), equalToIgnoringCase("rain"));
        assertThat(forecastWeatherCondition.weatherType.getLongDescription(), equalToIgnoringCase("light rain"));
    }

    @Test(expected = JsonSyntaxException.class)
    public void deserializeForecastWeatherConditions_invalidJson() throws Exception {
        gson.fromJson("{invalid json}", ForecastWeatherCondition.class);
    }
}