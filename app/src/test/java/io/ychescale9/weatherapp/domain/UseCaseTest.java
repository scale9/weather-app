package io.ychescale9.weatherapp.domain;

import org.junit.Before;
import org.junit.Test;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.ychescale9.weatherapp.TestSchedulerProvider;
import io.ychescale9.weatherapp.util.SchedulerProvider;

/**
 * Created by yang on 10/12/16.
 * Unit tests for the implementation of {@link UseCase}
 */
public class UseCaseTest {

    private final static String DUMMY_RESULT = "result";

    private TestObserver<String> testObserver;

    private UseCaseImpl useCase;

    @Before
    public void setUp() {
        testObserver = new TestObserver<>();
        useCase = new UseCaseImpl(new TestSchedulerProvider());
    }

    @Test
    public void getStream() {
        useCase.getStream().subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertValue(DUMMY_RESULT);
        testObserver.assertNoErrors();
    }

    private static class UseCaseImpl extends UseCase {

        UseCaseImpl(SchedulerProvider schedulerProvider) {
            super(schedulerProvider);
        }

        @Override
        protected Observable<String> createUseCase() {
            return Observable.just(DUMMY_RESULT);
        }
    }
}
