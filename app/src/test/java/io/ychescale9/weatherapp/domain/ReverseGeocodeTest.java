package io.ychescale9.weatherapp.domain;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.TestObserver;
import io.ychescale9.weatherapp.TestSchedulerProvider;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.SimpleAddress;
import io.ychescale9.weatherapp.util.GeocodingHelper;

import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 10/12/16.
 * Unit tests for the implementation of {@link ReverseGeocode}
 */
public class ReverseGeocodeTest {

    private final Coordinates coordinates = new Coordinates(37.8136, 144.9631);

    @Mock
    private SimpleAddress mockAddress1;

    @Mock
    private SimpleAddress mockAddress2;

    @Mock
    private GeocodingHelper mockGeocodingHelper;

    private TestObserver<SimpleAddress> testObserver;

    private ReverseGeocode reverseGeocode;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        // initialize objects
        testObserver = new TestObserver<>();
        reverseGeocode = new ReverseGeocode(
                new TestSchedulerProvider(),
                mockGeocodingHelper);

        // stub mock addresses
        when(mockAddress1.getLocality()).thenReturn("Melbourne");
        when(mockAddress2.getLocality()).thenReturn("Melbourne CBD");
    }

    @Test
    public void execute_oneResultFound() throws Exception {
        // assume the geocodingHelper returns a single address
        List<SimpleAddress> results = new ArrayList<>();
        results.add(mockAddress1);
        when(mockGeocodingHelper.getFromLocation(anyDouble(), anyDouble(), anyInt())).thenReturn(results);

        // when the use case is executed with coordinates
        reverseGeocode.setCoordinates(coordinates);
        reverseGeocode.getStream().subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();

        // should return the single address
        testObserver.assertValue(mockAddress1);
    }

    @Test
    public void execute_multipleResultsFound() throws Exception {
        // assume the geocodingHelper returns 2 addresses
        List<SimpleAddress> results = new ArrayList<>();
        results.add(mockAddress1);
        results.add(mockAddress2);
        when(mockGeocodingHelper.getFromLocation(anyDouble(), anyDouble(), anyInt())).thenReturn(results);

        // when the use case is executed with coordinates
        reverseGeocode.setCoordinates(coordinates);
        reverseGeocode.getStream().subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();

        // should return the single address
        testObserver.assertValue(mockAddress1);
    }

    @Test
    public void execute_ioException() throws Exception {
        // assume the geocodingHelper encounters an io exception
        when(mockGeocodingHelper.getFromLocation(anyDouble(), anyDouble(), anyInt())).thenThrow(new IOException());

        // when the use case is executed with coordinates
        reverseGeocode.setCoordinates(coordinates);
        reverseGeocode.getStream().subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();

        // expect an io exception
        testObserver.assertError(IOException.class);
    }

    @Test
    public void execute_illegalArgumentException() throws Exception {
        // assume the geocodingHelper encounters an illegal argument exception
        when(mockGeocodingHelper.getFromLocation(anyDouble(), anyDouble(), anyInt())).thenThrow(new IllegalArgumentException());

        // when the use case is executed with coordinates
        reverseGeocode.setCoordinates(coordinates);
        reverseGeocode.getStream().subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();

        // expect an illegal argument exception
        testObserver.assertError(IllegalArgumentException.class);
    }

    @Test
    public void execute_noResultsFound() throws Exception {
        // assume the geocodingHelper returns no results
        List<SimpleAddress> results = new ArrayList<>();
        when(mockGeocodingHelper.getFromLocation(anyDouble(), anyDouble(), anyInt())).thenReturn(results);

        // when the use case is executed with coordinates
        reverseGeocode.setCoordinates(coordinates);
        reverseGeocode.getStream().subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();

        // expect the error message indicating no address was found at the provided coordinates.
        testObserver.assertErrorMessage("No address found at the location " + coordinates.getLatitude() + "," + coordinates.getLongitude() + ".");
    }

    @Test
    public void execute_noCoordinatesProvided() throws Exception {
        // when the use case is executed with coordinates without setting coordinates
        reverseGeocode.getStream().subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();

        // expect the error message indicating no coordinates has been provided.
        testObserver.assertErrorMessage("Can't execute: no coordinates provided.");
    }
}