package io.ychescale9.weatherapp.domain;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.ychescale9.weatherapp.TestSchedulerProvider;
import io.ychescale9.weatherapp.data.cache.CurrentWeatherConditionCache;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.Temperature;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;
import io.ychescale9.weatherapp.data.model.WeatherType;
import io.ychescale9.weatherapp.data.repository.WeatherAppRepository;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 10/12/16.
 * Unit tests for the implementation of {@link LoadCurrentWeatherCondition}
 */
public class LoadCurrentWeatherConditionTest {

    private final Coordinates coordinates = new Coordinates(37.8136, 144.9631);

    private CurrentWeatherCondition dummyCurrentWeatherCondition;

    @Mock
    private WeatherAppRepository mockWeatherAppRepository;

    @Mock
    private CurrentWeatherConditionCache mockCurrentWeatherConditionCache;

    private TestObserver<CurrentWeatherCondition> testObserver;

    private LoadCurrentWeatherCondition loadCurrentWeatherCondition;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        // initialize / configure dummy objects
        dummyCurrentWeatherCondition = new CurrentWeatherCondition(
                1481351400000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 23),
                1481309446000L,
                1481362487000L);

        testObserver = new TestObserver<>();
        loadCurrentWeatherCondition = new LoadCurrentWeatherCondition(
                new TestSchedulerProvider(),
                mockWeatherAppRepository,
                mockCurrentWeatherConditionCache);
    }

    @Test
    public void execute() throws Exception {
        // given that loadCurrentWeatherCondition successfully returns a LoadCurrentWeatherCondition observable
        when(mockWeatherAppRepository.loadCurrentWeatherCondition(any(Coordinates.class))).thenReturn(Observable.just(dummyCurrentWeatherCondition));

        // when the use case is executed with coordinates
        loadCurrentWeatherCondition.setCoordinates(coordinates);
        loadCurrentWeatherCondition.getStream().subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();

        // should load current weather condition
        verify(mockWeatherAppRepository).loadCurrentWeatherCondition(coordinates);

        // should return the correct CurrentWeatherCondition
        testObserver.assertValue(dummyCurrentWeatherCondition);
    }

    @Test
    public void execute_invalidateCache() throws Exception {
        // given that loadCurrentWeatherCondition successfully returns a CurrentWeatherCondition observable
        when(mockWeatherAppRepository.loadCurrentWeatherCondition(any(Coordinates.class))).thenReturn(Observable.just(dummyCurrentWeatherCondition));

        // when the use case is executed with coordinates and invalidateCache set to true
        loadCurrentWeatherCondition.setCoordinates(coordinates);
        loadCurrentWeatherCondition.setInvalidateCache(true);
        loadCurrentWeatherCondition.getStream().subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();

        // should clear cache
        verify(mockCurrentWeatherConditionCache).clear();
        // should load current weather condition
        verify(mockWeatherAppRepository).loadCurrentWeatherCondition(coordinates);

        // should return the correct CurrentWeatherCondition
        testObserver.assertValue(dummyCurrentWeatherCondition);
    }

    @Test
    public void execute_noCoordinatesProvided() throws Exception {
        // when the use case is executed with coordinates without setting coordinates
        loadCurrentWeatherCondition.getStream().subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();

        // expect the error message indicating no coordinates has been provided.
        testObserver.assertErrorMessage("Can't execute: no coordinates provided.");
    }
}