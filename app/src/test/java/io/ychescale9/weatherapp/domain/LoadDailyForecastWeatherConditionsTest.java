package io.ychescale9.weatherapp.domain;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.ychescale9.weatherapp.TestSchedulerProvider;
import io.ychescale9.weatherapp.data.cache.DailyForecastWeatherConditionsCache;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.SummaryForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.Temperature;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;
import io.ychescale9.weatherapp.data.model.WeatherType;
import io.ychescale9.weatherapp.data.repository.WeatherAppRepository;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 11/12/16.
 * Unit tests for the implementation of {@link LoadDailyForecastWeatherConditions}
 */
public class LoadDailyForecastWeatherConditionsTest {

    private final Coordinates coordinates = new Coordinates(37.8136, 144.9631);

    private List<SummaryForecastWeatherCondition> dummyDailyForecastWeatherConditions;

    @Mock
    private WeatherAppRepository mockWeatherAppRepository;

    @Mock
    private DailyForecastWeatherConditionsCache mockDailyForecastWeatherConditionsCache;

    private TestObserver<List<SummaryForecastWeatherCondition>> testObserver;

    private LoadDailyForecastWeatherConditions loadDailyForecastWeatherConditions;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        // initialize / configure dummy objects
        dummyDailyForecastWeatherConditions = new ArrayList<>();
        dummyDailyForecastWeatherConditions.add(new SummaryForecastWeatherCondition(
                1481371400000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 28),
                new Temperature(TemperatureUnit.CELSIUS, 12)
        ));
        dummyDailyForecastWeatherConditions.add(new SummaryForecastWeatherCondition(
                1481371500000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 26),
                new Temperature(TemperatureUnit.CELSIUS, 13)
        ));
        dummyDailyForecastWeatherConditions.add(new SummaryForecastWeatherCondition(
                1481371800000L,
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 30),
                new Temperature(TemperatureUnit.CELSIUS, 14)
        ));

        testObserver = new TestObserver<>();
        loadDailyForecastWeatherConditions = new LoadDailyForecastWeatherConditions(
                new TestSchedulerProvider(),
                mockWeatherAppRepository,
                mockDailyForecastWeatherConditionsCache);
    }

    @Test
    public void execute() throws Exception {
        // given that loadDailyForecastWeatherConditions successfully returns a List<SummaryForecastWeatherCondition> observable
        when(mockWeatherAppRepository.loadDailyForecastWeatherConditions(any(Coordinates.class))).thenReturn(Observable.just(dummyDailyForecastWeatherConditions));

        // when the use case is executed with coordinates
        loadDailyForecastWeatherConditions.setCoordinates(coordinates);
        loadDailyForecastWeatherConditions.getStream().subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();

        // should load daily forecast weather conditions
        verify(mockWeatherAppRepository).loadDailyForecastWeatherConditions(coordinates);

        // should return the correct List<SummaryForecastWeatherCondition>
        testObserver.assertValue(dummyDailyForecastWeatherConditions);
    }

    @Test
    public void execute_invalidateCache() throws Exception {
        // given that loadDailyForecastWeatherConditions successfully returns a List<SummaryForecastCondition> observable
        when(mockWeatherAppRepository.loadDailyForecastWeatherConditions(any(Coordinates.class))).thenReturn(Observable.just(dummyDailyForecastWeatherConditions));

        // when the use case is executed with coordinates and invalidateCache set to true
        loadDailyForecastWeatherConditions.setCoordinates(coordinates);
        loadDailyForecastWeatherConditions.setInvalidateCache(true);
        loadDailyForecastWeatherConditions.getStream().subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();

        // should clear cache
        verify(mockDailyForecastWeatherConditionsCache).clear();
        // should load daily forecast weather conditions
        verify(mockWeatherAppRepository).loadDailyForecastWeatherConditions(coordinates);

        // should return the correct List<SummaryForecastWeatherCondition>
        testObserver.assertValue(dummyDailyForecastWeatherConditions);
    }

    @Test
    public void execute_noCoordinatesProvided() throws Exception {
        // when the use case is executed with coordinates without setting coordinates
        loadDailyForecastWeatherConditions.getStream().subscribeWith(testObserver);
        testObserver.awaitTerminalEvent();

        // expect the error message indicating no coordinates has been provided.
        testObserver.assertErrorMessage("Can't execute: no coordinates provided.");
    }
}