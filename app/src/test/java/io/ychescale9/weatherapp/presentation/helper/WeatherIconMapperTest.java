package io.ychescale9.weatherapp.presentation.helper;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.ychescale9.weatherapp.R;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.ForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.Temperature;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;
import io.ychescale9.weatherapp.data.model.WeatherType;
import io.ychescale9.weatherapp.util.Clock;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 11/12/16.
 * Unit tests for the implementation of {@link WeatherIconMapper}
 */
public class WeatherIconMapperTest {

    @Mock
    private Clock mockClock;

    private WeatherIconMapper weatherIconMapper;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        weatherIconMapper = new WeatherIconMapper(mockClock);

        // mock clock returns the real currentTimeMillis by default
        when(mockClock.getCurrentTimeMillis()).thenReturn(System.currentTimeMillis());
    }

    @Test
    public void getWeatherIconString_clearDay() throws Exception {
        long sunrise = 1481309446000L;
        long sunset = 1481362487000L;
        long currentTimeMillis = 1481351600000L;
        // given the current time is daytime
        when(mockClock.getCurrentTimeMillis()).thenReturn(currentTimeMillis);

        CurrentWeatherCondition currentWeatherCondition = new CurrentWeatherCondition(
                currentTimeMillis,
                new WeatherType(800, "Clear", "Clear day"),
                new Temperature(TemperatureUnit.CELSIUS, 25),
                sunrise,
                sunset);

        int iconResId = weatherIconMapper.getWeatherIconResId(currentWeatherCondition);

        assertThat(iconResId, is(R.string.weather_clear_day));
    }

    @Test
    public void getWeatherIconString_clearNight() throws Exception {
        long sunrise = 1481309446000L;
        long sunset = 1481351600000L;
        long currentTimeMillis = 1481371600000L;
        // given the current time is nighttime
        when(mockClock.getCurrentTimeMillis()).thenReturn(currentTimeMillis);

        CurrentWeatherCondition currentWeatherCondition = new CurrentWeatherCondition(
                currentTimeMillis,
                new WeatherType(800, "Clear", "Clear night"),
                new Temperature(TemperatureUnit.CELSIUS, 25),
                sunrise,
                sunset);

        int iconResId = weatherIconMapper.getWeatherIconResId(currentWeatherCondition);

        assertThat(iconResId, is(R.string.weather_clear_night));
    }

    @Test
    public void getWeatherIconString_clearNeutral() throws Exception {
        ForecastWeatherCondition forecastWeatherCondition = new ForecastWeatherCondition(
                1481351600000L,
                new WeatherType(800, "Clear", "Clear day"),
                new Temperature(TemperatureUnit.CELSIUS, 25));

        int iconResId = weatherIconMapper.getWeatherIconResId(forecastWeatherCondition);

        // neutral clear icon defaults to clear day icon
        assertThat(iconResId, is(R.string.weather_clear_day));
    }

    @Test
    public void getWeatherIconString_thunderstorm() throws Exception {
        ForecastWeatherCondition forecastWeatherCondition = new ForecastWeatherCondition(
                1481351600000L,
                new WeatherType(200, "Thunderstorm", "thunderstorm with light rain"),
                new Temperature(TemperatureUnit.CELSIUS, 25));

        int iconResId = weatherIconMapper.getWeatherIconResId(forecastWeatherCondition);

        assertThat(iconResId, is(R.string.weather_thunder));
    }

    @Test
    public void getWeatherIconString_drizzle() throws Exception {
        ForecastWeatherCondition forecastWeatherCondition = new ForecastWeatherCondition(
                1481351600000L,
                new WeatherType(301, "Drizzle", "drizzle"),
                new Temperature(TemperatureUnit.CELSIUS, 25));

        int iconResId = weatherIconMapper.getWeatherIconResId(forecastWeatherCondition);

        assertThat(iconResId, is(R.string.weather_drizzle));
    }

    @Test
    public void getWeatherIconString_rain() throws Exception {
        ForecastWeatherCondition forecastWeatherCondition = new ForecastWeatherCondition(
                1481351600000L,
                new WeatherType(500, "Rain", "light rain"),
                new Temperature(TemperatureUnit.CELSIUS, 25));

        int iconResId = weatherIconMapper.getWeatherIconResId(forecastWeatherCondition);

        assertThat(iconResId, is(R.string.weather_rainy));
    }

    @Test
    public void getWeatherIconString_snow() throws Exception {
        ForecastWeatherCondition forecastWeatherCondition = new ForecastWeatherCondition(
                1481351600000L,
                new WeatherType(600, "Snow", "light snow"),
                new Temperature(TemperatureUnit.CELSIUS, 25));

        int iconResId = weatherIconMapper.getWeatherIconResId(forecastWeatherCondition);

        assertThat(iconResId, is(R.string.weather_snowy));
    }

    @Test
    public void getWeatherIconString_atmosphere() throws Exception {
        ForecastWeatherCondition forecastWeatherCondition = new ForecastWeatherCondition(
                1481351600000L,
                new WeatherType(741, "Foggy", "foggy"),
                new Temperature(TemperatureUnit.CELSIUS, 25));

        int iconResId = weatherIconMapper.getWeatherIconResId(forecastWeatherCondition);

        assertThat(iconResId, is(R.string.weather_foggy));
    }

    @Test
    public void getWeatherIconString_clouds() throws Exception {
        ForecastWeatherCondition forecastWeatherCondition = new ForecastWeatherCondition(
                1481351600000L,
                new WeatherType(801, "Clouds", "few clouds"),
                new Temperature(TemperatureUnit.CELSIUS, 25));

        int iconResId = weatherIconMapper.getWeatherIconResId(forecastWeatherCondition);

        assertThat(iconResId, is(R.string.weather_cloudy));
    }

    @Test
    public void getWeatherIconString_unknownWeatherGroup() throws Exception {
        CurrentWeatherCondition currentWeatherCondition = new CurrentWeatherCondition(
                1481351600000L,
                new WeatherType(1000, "unknown", "unknown"),
                new Temperature(TemperatureUnit.CELSIUS, 25),
                1481309446000L,
                1481362487000L);

        int iconResId = weatherIconMapper.getWeatherIconResId(currentWeatherCondition);

        assertThat(iconResId, is(-1));
    }
}