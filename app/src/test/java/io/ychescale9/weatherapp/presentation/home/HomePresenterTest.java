package io.ychescale9.weatherapp.presentation.home;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.ychescale9.weatherapp.data.disk.SharedPreferencesManager;
import io.ychescale9.weatherapp.data.model.Coordinates;
import io.ychescale9.weatherapp.data.model.CurrentWeatherCondition;
import io.ychescale9.weatherapp.data.model.SimpleAddress;
import io.ychescale9.weatherapp.data.model.SummaryForecastWeatherCondition;
import io.ychescale9.weatherapp.data.model.Temperature;
import io.ychescale9.weatherapp.data.model.TemperatureUnit;
import io.ychescale9.weatherapp.data.model.WeatherType;
import io.ychescale9.weatherapp.domain.LoadCurrentWeatherCondition;
import io.ychescale9.weatherapp.domain.LoadDailyForecastWeatherConditions;
import io.ychescale9.weatherapp.domain.ReverseGeocode;
import io.ychescale9.weatherapp.util.RealClock;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by yang on 11/12/16.
 * Unit tests for the implementation of {@link HomePresenter}
 */
public class HomePresenterTest {

    private final int autoRefreshPollingIntervalMilliseconds = 30 * 60 * 1000; // 30 minutes
    private final int forecastDataUpdateIntervalMilliseconds = 3 * 60 * 60 * 1000; // 3 hours

    private final Coordinates coordinates = new Coordinates(37.8136, 144.9631);
    private final String cityName = "Malvern East";

    private CurrentWeatherCondition dummyCurrentWeatherCondition;

    private List<SummaryForecastWeatherCondition> dummyDailyForecastWeatherConditions;

    @Mock
    private HomeContract.View mockView;

    @Mock
    private SharedPreferencesManager mockSharedPreferencesManager;

    @Mock
    private ReverseGeocode mockReverseGeocode;

    @Mock
    private LoadCurrentWeatherCondition mockLoadCurrentWeatherCondition;

    @Mock
    private LoadDailyForecastWeatherConditions mockLoadDailyForecastWeatherConditions;

    @Mock
    private SimpleAddress mockAddress;

    private HomePresenter homePresenter;

    @Before
    public void setUp() throws Exception {
        // mock annotated objects
        MockitoAnnotations.initMocks(this);

        // initialize / configure dummy objects
        dummyCurrentWeatherCondition = new CurrentWeatherCondition(
                1481351400000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 23),
                1481309446000L,
                1481362487000L);

        dummyDailyForecastWeatherConditions = new ArrayList<>();
        dummyDailyForecastWeatherConditions.add(new SummaryForecastWeatherCondition(
                1481371400000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 28),
                new Temperature(TemperatureUnit.CELSIUS, 12)
        ));
        dummyDailyForecastWeatherConditions.add(new SummaryForecastWeatherCondition(
                1481371500000L,
                new WeatherType(801, "Cloud", "few cloud"),
                new Temperature(TemperatureUnit.CELSIUS, 26),
                new Temperature(TemperatureUnit.CELSIUS, 13)
        ));
        dummyDailyForecastWeatherConditions.add(new SummaryForecastWeatherCondition(
                1481371800000L,
                new WeatherType(800, "Clear", "clear sky"),
                new Temperature(TemperatureUnit.CELSIUS, 30),
                new Temperature(TemperatureUnit.CELSIUS, 14)
        ));

        homePresenter = new HomePresenter(
                mockView,
                mockSharedPreferencesManager,
                new RealClock(),
                mockReverseGeocode,
                mockLoadCurrentWeatherCondition,
                mockLoadDailyForecastWeatherConditions,
                autoRefreshPollingIntervalMilliseconds,
                forecastDataUpdateIntervalMilliseconds
        );

        // default stubs
        when(mockLoadCurrentWeatherCondition.getStream()).thenReturn(Observable.just(dummyCurrentWeatherCondition));
        when(mockLoadDailyForecastWeatherConditions.getStream()).thenReturn(Observable.just(dummyDailyForecastWeatherConditions));
        when(mockReverseGeocode.getStream()).thenReturn(Observable.just(mockAddress));
        when(mockAddress.getLocality()).thenReturn(cityName);
    }

    @Test
    public void loadCurrentAndForecastWeatherConditions() throws Exception {
        // set current location first
        homePresenter.updateCurrentLocation(coordinates);

        // should get stream
        verify(mockReverseGeocode).getStream();

        // load current weather and forecast
        homePresenter.loadCurrentAndForecastWeatherConditions(false);

        // should update UI to show initial loading view
        verify(mockView, atLeastOnce()).showInitialLoadingView();

        // should update UI with city name
        verify(mockView, atLeastOnce()).showCurrentCity(cityName);

        // show set coordinates
        verify(mockLoadCurrentWeatherCondition, atLeastOnce()).setCoordinates(any(Coordinates.class));
        // show set invalidateCache to false
        verify(mockLoadCurrentWeatherCondition, atLeastOnce()).setInvalidateCache(false);
        // should get stream
        verify(mockLoadCurrentWeatherCondition, atLeastOnce()).getStream();

        // should update UI with loaded current weather
        verify(mockView).showCurrentWeatherCondition(dummyCurrentWeatherCondition);

        // show set coordinates
        verify(mockLoadDailyForecastWeatherConditions, atLeastOnce()).setCoordinates(any(Coordinates.class));
        // should get stream
        verify(mockLoadDailyForecastWeatherConditions, atLeastOnce()).getStream();

        // should update UI with loaded list of forecast weather conditions
        verify(mockView, atLeastOnce()).showDailyForecastWeatherConditionsList(dummyDailyForecastWeatherConditions);
        // update UI with the selected forecast weather condition
        verify(mockView, atLeastOnce()).showForecastWeatherCondition(dummyDailyForecastWeatherConditions.get(0));
    }

    @Test
    public void loadCurrentWeatherCondition_serverError() throws Exception {
        // given that mockLoadCurrentWeatherCondition.getStream() returns an error observable
        when(mockLoadCurrentWeatherCondition.getStream()).thenReturn(Observable.<CurrentWeatherCondition>error(new Exception()));

        // set current location first
        homePresenter.updateCurrentLocation(coordinates);

        // load current weather and forecast
        homePresenter.loadCurrentAndForecastWeatherConditions(false);

        // should get stream
        verify(mockLoadCurrentWeatherCondition, atLeastOnce()).getStream();

        // should update UI indicating an error has occurred
        verify(mockView, atLeastOnce()).showCannotLoadWeatherDataError();
    }

    @Test
    public void loadForecastWeatherConditions_serverError() throws Exception {
        // given that mockLoadDailyForecastWeatherConditions.getStream() returns an error observable
        when(mockLoadDailyForecastWeatherConditions.getStream()).thenReturn(Observable.<List<SummaryForecastWeatherCondition>>error(new Exception()));

        // set current location first
        homePresenter.updateCurrentLocation(coordinates);

        // load current weather and forecast
        homePresenter.loadCurrentAndForecastWeatherConditions(false);

        // should get stream
        verify(mockLoadDailyForecastWeatherConditions, atLeastOnce()).getStream();

        // should update UI indicating an error has occurred
        verify(mockView, atLeastOnce()).showCannotLoadWeatherDataError();
    }

    @Test
    public void updateCurrentLocation() throws Exception {
        // set current location first
        homePresenter.updateCurrentLocation(coordinates);

        // should get stream
        verify(mockReverseGeocode).getStream();

        // should update UI with city name
        verify(mockView, atLeastOnce()).showCurrentCity(cityName);
    }

    @Test
    public void changeTemperatureUnit() throws Exception {
        // set temperature unit
        homePresenter.changeTemperatureUnit(TemperatureUnit.FAHRENHEIT);

        // should refresh view
        verify(mockView).refreshView();
    }

    @Test
    public void getTemperateUnit() throws Exception {
        // given sharedPreferencesManager returns the correct unit
        when(mockSharedPreferencesManager.getTemperatureUnit(TemperatureUnit.CELSIUS.getUnitName())).thenReturn(TemperatureUnit.FAHRENHEIT.getUnitName());

        // set temperature unit
        homePresenter.changeTemperatureUnit(TemperatureUnit.FAHRENHEIT);

        // should return correct unit
        assertThat(homePresenter.getTemperatureUnit(), is(TemperatureUnit.FAHRENHEIT));

        // given sharedPreferencesManager returns the correct unit
        when(mockSharedPreferencesManager.getTemperatureUnit(TemperatureUnit.CELSIUS.getUnitName())).thenReturn(TemperatureUnit.CELSIUS.getUnitName());

        // change temperature unit
        homePresenter.changeTemperatureUnit(TemperatureUnit.CELSIUS);

        // should return correct unit
        assertThat(homePresenter.getTemperatureUnit(), is(TemperatureUnit.CELSIUS));
    }

    @Test
    public void destroy() throws Exception {
        homePresenter.destroy();
    }
}