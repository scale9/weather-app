package io.ychescale9.weatherapp;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import io.ychescale9.weatherapp.util.SchedulerProvider;

/**
 * Created by yang on 14/12/16.
 * SchedulerProvider for unit tests.
 */
public class TestSchedulerProvider extends SchedulerProvider {

    @Override
    public Scheduler ui() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler io() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler computation() {
        return Schedulers.trampoline();
    }
}
