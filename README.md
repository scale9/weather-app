# Weather App

The app has the following features:

* Shows the current weather info for the current location
* Shows a scrollable list of weather forecasts at the bottom
* The list of forecasts can be selected to show more details (the weather summary description)
* Users can change temperature unit (Celsius or Fahrenheit) from the overflow menu
 
## Environments

This 2 product flavors:

* **dev** - app's weather data is driven by hardcoded data (JSON string)
* **prod** - app's weather data is driven by real data coming from the [OpenWeatherMap](https://openweathermap.org)

## Testing

Tests are in the following directories: 

* **test** - junit tests
* **androidTest** - currently there are only a few instrumentation tests for shared preferences

Tests can be run by creating the Run/Debug configurations over the above directories from Android Studio.

## Architecture / Design Decisions
* The app follows the clean architecture and MVP for the presentation layer, implemented with RxJava
* There are caches (in-memory) of the loaded current weather and forecasts data, which expire based on how frequently the source data is likely to change from the server
* The app will automatically re-load data when significant change in location is detected
* DI is used to provide 2 different environments in terms of data source
* In the future we'll have automated UI tests, also set up in a mock environment, and use a mock web server to simulate network responses covering both happy and error cases
